<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      public function features()
    {
        return $this->hasMany('App\Feature');
    }
      public function faqs()
    {
        return $this->hasMany('App\Faq');
    }
      public function drivers()
    {
        return $this->hasMany('App\Driver');
    }
     public function productimages()
    {
        return $this->hasMany('App\Productimage');
    }
    
    public function brand()
    {
      return $this->belongsTo('App\Brand');
    }
}
