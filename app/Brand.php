<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
     public function brandimages()
    {
        return $this->hasMany('App\Brandimage');
    }
    public function products()
    {
    	return $this->hasMany('App\Product');
    }

}
