<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Brand;
use App\Solution;
use App\Product;
use App\Feature;
use App\Faq;
use App\Driver;
use App\Productimage;
use Redirect;
use Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Products')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function index()
    {
        $products=Product::all();
        // dd($products);
        return view('admin.product.list')->with('products',$products);
    }
    public function search(Request $request)
    {
        $products=Product::where('name','LIKE','%'.$request->key.'%')->get();
        // dd($products);
        return view('admin.product.list')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function create()
    {
        return view('admin.product.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::where('id',$id)->with('features')->with('faqs')->with('drivers')->with('productimages')->first();
        return view('admin.product.edit')->with('product',$product);
    }
   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $links=Driver::where('product_id',$id)->get()->pluck('file');
        foreach ($links as $l) {
            unlink('admin-assets/uploads'.'/'.$l);
        }
        if(Product::where('id',$id)->first()->brochure!=null)
            unlink('admin-assets/uploads'.'/'.Product::where('id',$id)->first()->brochure);
        Product::where('id',$id)->delete();
        $products=Product::all();
        return Redirect::to('/admin/products');
      

    }

    public function addBasics(Request $request)
    {       
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'brand_id'=>'required|exists:brands,id',
            'solution_id'=>'required|exists:solutions,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }

       $p= new Product;
       $p->name=$request->name;
       $p->description=$request->description;
       $p->brand_id=$request->brand_id;
       $p->solution_id=$request->solution_id;
       $p->priority=$request->priority;
       $file=$request->file('brochure');
       if($file!=null)
       {
           $validator = Validator::make($request->all(), [
            'brochure'=>'mimes:pdf',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }   
           $filename=time() . '.' .$file->getClientOriginalExtension();
           $path='admin-assets/uploads';
           $file->move($path,$filename);
           $p->brochure=$filename;
       }
      // return $p;
       $p->save();
       // dd($p);
       return $p->id; 
    }
    public function editBasics(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'brand_id'=>'required|exists:brands,id',
            'solution_id'=>'required|exists:solutions,id',
            ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }      
       $p=Product::where('id',$request->id)->first();
       $p->name=$request->name;
       $p->description=$request->description;
       $p->brand_id=$request->brand_id;
       $p->solution_id=$request->solution_id;
       $p->priority=$request->priority;
       $file=$request->file('brochure');
       if($file!=null)
       {
           $validator = Validator::make($request->all(), [
            'brochure'=>'mimes:pdf',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }      
           $filename=time() . '.' .$file->getClientOriginalExtension();
           $path='admin-assets/uploads';
           $file->move($path,$filename);
           unlink($path.'/'.$p->brochure);
           $p->brochure=$filename;
       }
       $p->save();
       // dd($p);
       return $p->id; 
    }
    public function getBrandsAndSolutions()
    {
        $brands=Brand::All();
        $solutions=Solution::All();
        // return "asd";

        return response()->json([
            'brands' => $brands,
            'solutions' => $solutions
            ]);
    }
    public function saveFeatures(Request $request)
    {
          $validator = Validator::make($request->all(), [
            'features.*.title' => 'required',
            'features.*.description' => 'required',
        ]);
          if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }
        Feature::where('product_id',$request->id)->delete();
        Feature::insert($request->features);
        return Feature::where('product_id',$request->id)->get();  
    }
    public function saveSpecification(Request $request)
    {
        Product::where('id',$request->id)->update(['specification'=>$request->specification]);
        return $request->specification;
    }
    public function savePurpose(Request $request)
    {
        
        Product::where('id',$request->id)->update(['purpose'=>$request->purpose]);
        return $request->purpose;
    }
    public function saveFaqs(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'faqs.*.question' => 'required',
            'faqs.*.answer' => 'required',
        ]);
          if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        }
        Faq::where('product_id',$request->id)->delete();
        Faq::insert($request->faqs);
        return Faq::where('product_id',$request->id)->get(); 
    }
    public function addDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'file'=>'required|mimes:rar,exe,zip'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        } 
        $d= new Driver;
        $d->title=$request->title;
        $file=$request->file('file');
        $filename=time() . '.' .$file->getClientOriginalExtension();
        $path='admin-assets/uploads';
        $file->move($path,$filename);
        $d->file=$filename;
        $d->product_id=$request->id;
        $d->save();
        return Driver::all();

    }
    public function saveDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'file'=>'required|mimes:rar,exe,zip'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 404);
        } 
        $d=Driver::where('id',$request->driver_id)->first();
        $d->title=$request->title;
        $file=$request->file('file');
        if($file!=null)
        {

            $filename=time() . '.' .$file->getClientOriginalExtension();
            $path='admin-assets/uploads';
            $file->move($path,$filename);
            unlink($path.'/'.$d->file);
            $d->file=$filename;
        }
        $d->save();
        return Driver::all();
    }
    public function removeDriver(Request $request)
    {
        $path='admin-assets/uploads';
        $file=Driver::where('id',$request->driver_id)->first()->file;
        unlink($path.'/'.$file);
        Driver::where('id',$request->driver_id)->delete();
        return Driver::all();
    }
     public function addImage(Request $request,$id){
        if($request->hasFile('files')){
            $images = $request->file('files');
            
        }
       
        $count = 0;
        foreach($images as $image){
            $Limage = new Productimage;
            $filename = time().'a'.$count++ . '.' . $image->getClientOriginalExtension();
            $location = public_path('admin-assets/uploads/');
            $image->move($location,$filename);
            $Limage->image = $filename;
            $Limage->product_id=$id;
            $Limage->save();
        }
        
        // $request->session()->flash('success', 'Images Uploaded');
        return Productimage::where('product_id',$id)->get();
    }
    public function removeImage(Request $request)
    {
        unlink(public_path('admin-assets/uploads').'/'.Productimage::where('id',$request->id)->first()->image);
        Productimage::where('id',$request->id)->delete();
        return Productimage::where('product_id',$request->product_id)->get();
    }
    public function changeFeatured($id)
    {
        if(Product::where('id',$id)->first()->featured==0)
        {
            Product::where('id',$id)->update(['featured'=>1]);   
        }
        else
            Product::where('id',$id)->update(['featured'=>0]);  
        return Redirect::to('admin/products');
        // $products=Product::all();
        // // dd($products);
        // return view('admin.product.list')->with('products',$products);
    }
    public function changeStatus($id)
    {
        if(Product::where('id',$id)->first()->status=='active')
        {
            Product::where('id',$id)->update(['status'=>'dormant']);   
        }
        else
            Product::where('id',$id)->update(['status'=>'active']);  
        return Redirect::to('admin/products');
        // $products=Product::all();
    }
}
