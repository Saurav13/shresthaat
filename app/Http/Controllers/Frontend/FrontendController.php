<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\AdditionalInfo;
use App\Service;
use App\Quotation;
use App\Contact;
use App\Product;
use App\Album;
use App\ServiceSupportMessage;
use App\Mail\ServiceSupportEmail;
use Mail;

class FrontendController extends Controller
{

    public function services(){
        $services = Service::orderBy('updated_at','desc')->paginate(4);
        return view('frontend.services')->with('services',$services);        
    }

    public function sendServiceMessage(Request $request){
        

        if ($request->ajax()){
            $validator = Validator::make($request->all(), [
                'name'=>'required|max:255',
                'email'=> 'required|email',
                'phone'=> 'required',
                'subject'=> 'required',
                'message'=> 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()]);
            }

            $service_message = new ServiceSupportMessage;

            $service = Service::find($request->id);
            
            //no need for if condition anymore
            if(!$service)
                $service_message->sentTo = 'support@stp.com';
            else
                $service_message->sentTo = $service->email;

            $service_message->name = $request->name;
            $service_message->email = $request->email;
            $service_message->phone = $request->phone;
            $service_message->subject = $request->subject;
            $service_message->message = $request->message;

            Mail::to($service_message->sentTo)->send(new ServiceSupportEmail($service_message));      

            $service_message->save();
            
            return;
        }

        abort(404);
    }

    public function about()
    {
        $images = [];
        $about = json_decode(AdditionalInfo::where('alias','about')->first()->info);
        $albums = Album::all();
        foreach($albums as $album){
            foreach($album->album_images()->limit(3)->get() as $image){
                $images []= $image;
            }
        }
        return view('frontend.about')->with('about',$about)->with('images',$images);
    }

    public function contact(){
        $contact = json_decode(AdditionalInfo::where('alias','contact')->first()->info);
        return view('frontend.contact')->with('contact',$contact);        
    }

    public function contactSend(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:255',
            'email'=> 'required|email',
            'phone'=> 'required',
            'subject'=> 'required',
            'message'=> 'required',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('error', 'Please Provide All Necessary Information');
            return redirect()->route('contact');
        }
        $contact_message = new Contact;
        $contact_message->name = $request->name;
        $contact_message->email = $request->email;
        $contact_message->phone = $request->phone;
        $contact_message->subject = $request->subject;
        $contact_message->message = $request->message;
        $contact_message->save();
        
        $request->session()->flash('success', "We'll soon respond to you");
        return redirect()->route('contact');        
    }

    public function getQuote(){
        $prods = Product::all();
        $products=array();
        foreach ($prods as $p) {
            $product = (object)null;
            $product->text = $p->name;
            $product->id = $p->id;
            if($p->productimages->count()>0)
                $product->data = $p->productimages->first()->image;
            else
                $product->data="";
            $products []= $product;
        }
        return view('frontend.quotation')->with('products',json_encode($products));
    }

    public function sendQuote(Request $request){
        $validator = Validator::make($request->all(), [
            'firstname'=>'required|max:255',
            'lastname'=>'required|max:255',
            'email'=> 'required|email',
            'phone'=> 'required',
            'message'=> 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax())
                return response()->json(['errors'=>$validator->errors()]);
            $request->session()->flash('error', 'Please Provide All Necessary Information');
            return redirect()->route('getQuote');
        }
        $quote = new Quotation;
        $quote->customer_name = $request->firstname.' '.$request->lastname;
        $quote->email = $request->email;
        $quote->phone_number = $request->phone;
        $quote->quote_items = $request->products;
        $quote->message = $request->message;
       
        $quote->save();

        if ($request->ajax()){
            return;
        }

        $request->session()->flash('success', "We'll soon respond to your quotation");
        return redirect()->route('getQuote');
    }

    public function albums(){
        $albums = Album::orderBy('created_at','desc')->get();
        $year_albums = [];
        foreach($albums as $album){
            if(count($album->album_images)>0)
                $year_albums[$album->year] []= $album;
        }
        return view('frontend.gallery')->with('year_albums',$year_albums);
    }

    public function album($slug){
        $album = Album::where('slug',$slug)->first();
        if(!$album)
            abort(404);
        return view('frontend.singlealbum')->with('album',$album);
    }
}
