<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Solution;
use App\Product;

class ProductController extends Controller
{

	public function index(Request $request)
	{
		$cid=$request->cid;
		$bid=$request->bid;
		$filtype=$request->filtype;
		$breadcrumbs=$this->getBreadcrumbs($cid,$filtype);

		// dd($breadcrumbs);
		$products=$this->getProducts($cid,$filtype);
		usort($products, function($a, $b)	
		{
		    return strcmp($a->priority, $b->priority);
		});
		// dd($products);
		$category=$this->getCategory($cid,$filtype);
		$childs=$this->getChilds($cid,$filtype);

		// dd($products);
		// dd(sizeof($breadcrumbs));
		return view('frontend.product',compact('breadcrumbs','products','category','childs','filtype','bid'));
	}

	public function search(Request $request)
	{
		$bid=$request->bid;
		$products=Product::where('name','LIKE','%'.$request->key.'%')->get();
		$breadcrumbs=json_decode($request->breadcrumbs);
		$category=json_decode($request->category);
		$filtype=$request->filtype;
		// dd($request->filtype);
		$key=$request->key;
		return view('frontend.search',compact('breadcrumbs','products','category','filtype','bid','key'));
	}

	public function brand($name)
	{
		$brand=Brand::where('name',$name)->with('brandimages')->first();
		if(!$brand) abort(404);

		$filtype="brand";
		$bid=$brand->id;
		// dd($brand);
		return view('frontend.brand',compact('brand','filtype','bid'));
	}

	public function singleProduct(Request $request)
	{
		$cid="";
		if($request->filtype=="brand")
			$cid=Product::where('id',$request->id)->first()->brand_id;
		else
			$cid=Product::where('id',$request->id)->first()->solution_id;
		dd($cid);
		// if(!$cid) abort(404);

		$bid=$request->bid;
		$breadcrumbs=$this->getBreadcrumbs($cid,$request->filtype);
		$filtype=$request->filtype;
		$product=Product::where('id',$request->id)->with('features')->with('faqs')->with('drivers')->with('brand')->first();
		$bid=0;
		$pid=$product->brand_id;
		while($pid!=0)
		{
			$bid=$pid;
			$pid=Brand::where('id',$pid)->first()->parent_id;
		}
		// dd($bid);
		$brandname=Brand::where('id',$bid)->first()->name;
		return view('frontend.singleproduct',compact('breadcrumbs','product','filtype','bid','brandname'));
		// dd($product);
	}

	private function getBreadcrumbs($cid,$filtype)
	{
		$bc=array();
		$pid=$cid;
		while($pid!=0)
		{
			$cat=(object)[];
			if($filtype=='brand')
				$cat=Brand::where('id',$pid)->first();
			else if($filtype=='solution')
				$cat=Solution::where('id',$pid)->first();
			else
				abort(404);
			if(!$cat) abort(404);
			array_unshift($bc, $cat);
			$pid=$cat->parent_id;
		}

		return $bc;
	}
	private function getProducts($cid,$filtype)
	{
		$arr=array();
		$cat=(object)[];
		if($filtype=='brand')
		{	
			$cat=Brand::where('id',$cid)->first();
			if(!$cat) abort(404);
			if(Brand::where('parent_id',$cat->id)->count()==0)
			{

				$pds=Product::where('brand_id',$cat->id)->where('status','active')->with('productimages')->get();
				// dd($cat->id);
				foreach ($pds as $p) {
					array_push($arr, $p);
				}
				
				return $arr;
			}
			else
			{
				$childs=Brand::where('parent_id',$cat->id)->get();
				foreach ($childs as $c) {
					$a=$this->getProducts($c->id,$filtype);
					$arr=array_merge($arr,$a);
				}

				
			}
		}
		else
		{
			$cat=Solution::where('id',$cid)->first();
			if(!$cat) abort(404);
			if(Solution::where('parent_id',$cat->id)->count()==0)
			{
				$pds=Product::where('solution_id',$cat->id)->where('status','active')->with('productimages')->get();
				// dd($cat->id);
				foreach ($pds as $p) {
					array_push($arr, $p);
				}
				
				return $arr;
			}
			else
			{
				$childs=Solution::where('parent_id',$cat->id)->get();
				foreach ($childs as $c) {
					$a=$this->getProducts($c->id,$filtype);
					$arr=array_merge($arr,$a);
				}

				
			}
		}
		return $arr;
	}
	private function getCategory($cid,$filtype)
	{
		if($filtype=='brand')
			return Brand::where('id',$cid)->first();
		else if($filtype=='solution')
			return Solution::where('id',$cid)->first();
	}
	private function getChilds($cid,$filtype)
	{
		if($filtype=='brand')
			return Brand::where('parent_id',$cid)->get();
		else if($filtype=='solution')
			return Solution::where('parent_id',$cid)->get();
	}
}
