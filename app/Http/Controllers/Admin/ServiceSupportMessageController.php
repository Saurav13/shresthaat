<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceSupportMessage;
use App\Mail\ServiceMessageReply;
use App\Rules\NoEmptyContent;
use Mail;
use Auth;

class ServiceSupportMessageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Service Support Messages')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index(){
        $service_messages = ServiceSupportMessage::orderBy('created_at','desc')->paginate(10);
        return view('admin.service_messages.index')->with('service_messages',$service_messages)->with('state','all');
    }

    public function unseenMsg(){
        $service_messages = ServiceSupportMessage::where('seen','0')->orderBy('created_at','desc')->paginate(10);
        return view('admin.service_messages.index')->with('service_messages',$service_messages)->with('state','unseen');
    }

    public function show($id)
    {
        $service_message = ServiceSupportMessage::find($id);

        if(!$service_message)
            abort(404);

        if(!$service_message->seen){
            $service_message->seen = 1;
            $service_message->save();
        }
        
        return view('admin.service_messages.show')->with('service_message',$service_message);
    }

    public function destroy($id,Request $request)
    {
        $service_message = ServiceSupportMessage::find($id);
        $service_message->delete();

        $request->session()->flash('success', 'Message deleted');
        return redirect()->route('service-support-messages.index');
    }

    public function reply($id)
    {
        $service_message = ServiceSupportMessage::find($id);
        
        return view('admin.service_messages.reply')->with('service_message',$service_message);
    }

    public function replySend(Request $request, $id)
    {   
        $request->validate(array(
            'subject'=>'max:255',
            'message'=> ['required', new NoEmptyContent]
        ));

        $subject = $request->subject;
        $attachment = '';
        if($request->hasFile('attachment')){
            $attachment = $request->file('attachment');
        }

        $service_message = ServiceSupportMessage::find($id);

        Mail::to($service_message->email)->send(new ServiceMessageReply($request->message,$attachment,$subject));            

        $request->session()->flash('success', 'Reply sent.');
    
        return redirect()->route('service-support-messages.show',$service_message->id);
    }

    public function getUnseenSSMsgCount(Request $request){
        
        if ($request->ajax()){
            $sno = ServiceSupportMessage::where('seen','0')->count();
            return response()->json(['sno' => $sno]);
        }
        abort(404);
    }

    public function getUnseenSSMsg(Request $request){
        if ($request->ajax()){
            $messages = ServiceSupportMessage::where('seen','0')->latest()->limit(5)->get();
            $sno = ServiceSupportMessage::where('seen','0')->count();
            $view = view('admin.partials.snotis')->with('messages',$messages)->with('sno',$sno)->render();
            
            return response()->json(['html' => $view, 'sno' => $sno]);
        }
        abort(404);
    }
}
