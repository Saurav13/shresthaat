<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use App\Quotation;
use App\ServiceSupportMessage;
use App\Contact;
use App\Subscriber;
use App\User;
use App\Service;

class AdminController extends Controller
{
    
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {

            if (Auth::user()->roles()->where('title', '=', 'Dashboard')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalProducts=Product::count();
        $totalQuotations=Quotation::count();
        $totalSupports=ServiceSupportMessage::count();
        $totalMessages=Contact::count();
        $totalSubscribers=Subscriber::count();
        $totalStaffs=User::count();
        $totalServices=Service::count();
        $quotations=Quotation::latest('created_at')->limit(5)->get();
        $supportmessages=ServiceSupportMessage::latest('created_at')->limit(5)->get();
        $messages=Contact::latest('created_at')->limit(5)->get();
        
        // dd($quotation);
        return view('admin.dashboard',compact('totalServices','totalStaffs','totalSubscribers','totalMessages','totalSupports','totalQuotations','totalProducts','quotations','supportmessages','messages'));
    }

}
