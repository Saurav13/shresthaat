<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quotation;
use App\Mail\QuotationReply;
use App\Rules\NoEmptyContent; 
use Mail;
use Auth;
use PDF;

class QuotationController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Quotations')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index(){
        $quotations = Quotation::orderBy('created_at','desc')->paginate(10);
        return view('admin.quotations.index')->with('quotations',$quotations)->with('state','all');
    }

    public function unseenQuotes(){
        $quotations = Quotation::where('seen','0')->orderBy('created_at','desc')->paginate(10);
        return view('admin.quotations.index')->with('quotations',$quotations)->with('state','unseen');
    }

    public function show($id)
    {
        $quotation = Quotation::find($id);

        if(!$quotation)
            abort(404);

        if(!$quotation->seen){
            $quotation->seen = 1;
            $quotation->save();
        }
        
        return view('admin.quotations.show')->with('quotation',$quotation);
    }

    public function destroy($id,Request $request)
    {
        $quotation = Quotation::find($id);
        $quotation->delete();

        $request->session()->flash('success', 'Quotation deleted');
        return redirect()->route('quotations.index');
    }

    public function reply($id)
    {
        $quotation = Quotation::find($id);
        
        return view('admin.quotations.reply')->with('quotation',$quotation);
    }

    public function replySend(Request $request, $id)
    {   
        $request->validate(array(
            'subject'=>'max:255',
            'message'=> ['required', new NoEmptyContent]
        ));

        $subject = '';

        if($request->subject){
            $subject = $request->subject;
        }

        $quotation = Quotation::find($id);
        $view = view('admin.quotations.quote')->with('quotation',$quotation)->render();
        $pdf = PDF::loadHTML($view);

        Mail::to($quotation->email)->send(new QuotationReply($quotation->customer_name,$request->message,$pdf->output(),$subject));           

        $request->session()->flash('success', 'Reply sent.');
    
        return redirect()->route('quotations.show',$quotation->id);
    }

    public function getUnseenQuoteCount(Request $request){
        
        if ($request->ajax()){
            $qno = Quotation::where('seen','0')->count();
            return response()->json(['qno' => $qno]);
        }
        abort(404);  
    }

    public function getUnseenQuote(Request $request){
        if ($request->ajax()){
            $quotes = Quotation::where('seen','0')->latest()->limit(5)->get();
            $qno = Quotation::where('seen','0')->count();
            $view = view('admin.partials.qnotis')->with('quotes',$quotes)->with('qno',$qno)->render();

            return response()->json(['html' => $view, 'qno' => $qno]);
        }
        abort(404);    
    }
}
