<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use Session;
use Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Services')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function index(){
        $services = Service::orderBy('updated_at','desc')->get(); 
        return view('admin.services.index')->with('services',$services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(array(
            'title'=>'required|max:255',
            'name'=>'required',
            'email'=>'required|email',
            'photo'=>'mimes:jpeg,bmp,png',
            'description'=>'required'
        ));

        $service = new Service;
        $service->name = $request->name;
        $service->email = $request->email;
        $service->address = $request->address;
        $service->phone = $request->phone;

        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('service_images/');
            $image->move($location,$filename);
            $service->photo= $filename;
        }

        $service->title = $request->title;
        $service->description = $request->description;
        $service->save();

        Session::flash('success', 'Service added.');
        return redirect()->route('services.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        if(!$service)
            abort(404);
        return view('admin.services.edit')->with('service',$service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);

        $request->validate(array(
            'title'=>'required|max:255',
            'name'=>'required',
            'email'=>'required|email',
            'photo'=>'mimes:jpeg,bmp,png',
            'description'=>'required',
            'address'=>'required',
            'phone'=>'required'
        ));

        $service->name = $request->name;
        $service->email = $request->email;
        $service->address = $request->address;
        $service->phone = $request->phone;

        if($request->hasFile('photo')){
            if($service->photo)
                unlink(public_path('service_images/'.$service->photo));

            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('service_images/');
            $image->move($location,$filename);
            $service->photo = $filename;
        }

        $service->title = $request->title;
        $service->description = $request->description;
        $service->save();

        Session::flash('success', 'Service updated.');
        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if($id == 1){
            Session::flash('error', 'You cannot delete this Service.');
            return redirect()->route('services.index');
        }
        
        $service = Service::find($id);
        if($service->photo)
            unlink(public_path('service_images/'.$service->photo));
        $service->delete();

        Session::flash('success', 'Service deleted.');
        return redirect()->route('services.index');
    }
}
