<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\LandingImage;
use Illuminate\Support\Facades\Validator;
use App\AdditionalInfo;
use App\Brand;
use App\Solution;
use App\Testimonial;
use App\Product;
use App\Subscriber;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands=Brand::where('parent_id',0)->get();
        $landing_images = LandingImage::all();
        $about = json_decode(AdditionalInfo::where('alias','about')->first()->info);
        $testimonials = Testimonial::all()->sortByDesc('updated_at');
        $featured_products=Product::where('featured',1)->where('status','active')->get();
        $final_featured_products = [];

        $index = [];
        if(count($featured_products)>4){
            $i= rand(0,count($featured_products)-1);
            for($j=0;$j<4;$j++){
                while(in_array($i, $index)){
                    $i= rand(0,count($featured_products)-1);
                }
                $index []= $i;
            }
        }
        else
            $final_featured_products = $featured_products;

        foreach($index as $i){
            $final_featured_products []= $featured_products[$i];
        }
        return view('frontend.landing')->with('landing_images',$landing_images)->with('about',$about)->with('testimonials',$testimonials)->with('brands',$brands)->with('featured',$final_featured_products);
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subemail'=> 'required|email'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('error', 'Valid Email is required.');
            return redirect()->route('about');
        }

        if (! Subscriber::where('email', '=', $request->subemail)->exists()) {

            $subscriber = new Subscriber();
            $subscriber->email = $request->subemail;
            $subscriber->token = Str::random(60);
            $subscriber->save();
            Session::flash('success','Thank you for subscribing to us.');
        }
        else
            Session::flash('error','Looks like you are already in our subscription list.');
        return redirect()->route('about');
    }

    public function unsubscribe($token){
        return view('frontend.unsubscribe')->with('token',$token);
    }

    public function unsubscribed($token)
    {
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        $subscriber->delete();
        Session::flash('success','You are no longer subscribed to us.');
        return redirect()->route('home');
    }
}
