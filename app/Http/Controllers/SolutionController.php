<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solution;
use App\Product;
use Auth;

class SolutionController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Solutions')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
     public function index()
    {
        return view('admin.solutions.list');
    }
    public function getSolutions()
    {
        return Solution::with('products')->get();
    }
    public function getSingleSolution(Request $request)
    {
        return Solution::where('id',$request->id)->first();
    }
    public function addSolution(Request $request)
    {
        $request->validate([
            'name' => 'required'
           ]);
        $b=new Solution;
        $b->name=$request->name;
        $b->description=$request->description;
        $b->parent_id=$request->parent_id;
        $b->save();
        if(Product::where('solution_id',$request->parent_id)->count()>0)
        {
            Product::where('solution_id',$request->parent_id)->update(['solution_id'=>$b->id]);
        }
        return Solution::with('products')->get();
    }
     public function hasChild(Request $request)
    {
        if(Product::where('solution_id',$request->parent_id)->count()>0)
            return "hasChild";
        else 
            return $this->addSolution($request);
    }
    public function createSolution(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $b=new Solution;
        $b->name=$request->name;
        $b->description=$request->description;
        $b->parent_id=$request->parent_id;
        $b->save();
        $request->session()->flash('success', 'Solution added.'); 
        return view('admin.solutions.list');
    }
    public function editSolution(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $b= Solution::where('id',$request->id)->first();
        $b->name=$request->name;
        $b->description=$request->description;
        $b->save();
        if(Product::where('solution_id',$request->parent_id)->count()>0)
        {
            Product::where('solution_id',$request->parent_id)->update(['solution_id'=>$b->id]);
        }
        return Solution::with('products')->get();
    }
    private function deletebranch($id)
    {
        if(Solution::where('parent_id',$id)->count()==0)
        {
            Solution::where('id',$id)->delete();
        }
        else
        {
            $solutions=Solution::where('parent_id',$id)->get();
            foreach ($solutions as $b) {
                $this->deletebranch($b->id);    
            }
        }
        
    }
    public function deleteSolution(Request $request)
    {
        $this->deletebranch($request->id);
        Solution::where('id',$request->id)->delete();
        return Solution::with('products')->get();
    }
}
