<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Brandimage;
use Session;
use Redirect;
use App\Product;
use Auth;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->roles()->where('title', '=', 'Brands')->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function index()
    {
    	return view('admin.brands.list');
    }
    public function getBrands()
    {
    	return Brand::with('products')->get();
    }
    public function getSingleBrand(Request $request)
    {
    	return Brand::where('id',$request->id)->first();
    }
    public function addBrand(Request $request)
    {
        $request->validate([
            'name' => 'required',
           
        ]);

    	$b=new Brand;
    	$b->name=$request->name;
    	$b->description=$request->description;
    	$b->parent_id=$request->parent_id;

    	if($request->parent_id==0)
        {
            $file=$request->file('logo');
            if($file!=null)
            {
                $filename=time() . '.' .$file->getClientOriginalExtension();
                $path='admin-assets/uploads';
                $file->move($path,$filename);
                $b->logo=$filename;
            }
        }
        $b->save();
    	if(Product::where('brand_id',$request->parent_id)->count()>0)
        {
            Product::where('brand_id',$request->parent_id)->update(['brand_id'=>$b->id]);
        }
        return Brand::with('products')->get();
    }
    public function hasChild(Request $request)
    {
        if(Product::where('brand_id',$request->parent_id)->count()>0)
            return "hasChild";
        else 
            return $this->addBrand($request);
    }

    public function editBrand(Request $request)
    {
        
          $request->validate([
            'name' => 'required'
        ]);
    	$b= Brand::where('id',$request->id)->first();
    	$b->name=$request->name;
    	$b->description=$request->description;
    	if($request->parent_id==0)
        {

            $file=$request->file('logo');
            if($file!=null)
            {
                $filename=time() . '.' .$file->getClientOriginalExtension();
                $path='admin-assets/uploads';
                unlink($path.'/'.$b->logo);
                
                $file->move($path,$filename);
                $b->logo=$filename;
            }
        }
        $b->save();
    	return Brand::with('products')->get();
    }
    private function deletebranch($id)
    {
        if(Brand::where('parent_id',$id)->count()==0)
        {
            Brand::where('id',$id)->delete();
        }
        else
        {
            $brands=Brand::where('parent_id',$id)->get();
            foreach ($brands as $b) {
                $this->deletebranch($b->id);    
            }
        }
        
    }
    public function deleteBrand(Request $request)
    {
        $this->deletebranch($request->id);
        Brand::where('id',$request->id)->delete();
        return Brand::with('products')->get();
    }
    public function createMainBrand(Request $request)
    {
        return view('admin.brands.list');
        dd("asd");
        // $b= new Brand;
        // $b->name=$request->name;
        // $b->save();
        // return view('admin.brands.form')->with('brand',$b);
    }
    public function editMainBrand($id)
    {
        $b=Brand::where('id',$id)->first();
        $images=$b->brandimages;
        // dd($b->description);
        return view('admin.brands.form')->with('brand',$b)->with('images',$images);
    }
    public function createBrand(Request $request)
    {
         $request->validate([
            'name' => 'required',
            'logo'=>'mimes:jpeg,bmp,png'
        ]);
        $b= new Brand;
        $b->name=$request->name;
        $b->parent_id=0;
        $file=$request->file('logo');
        if($file!=null)
        {
            $filename=time() . '.' .$file->getClientOriginalExtension();
            $path='admin-assets/uploads';
            $file->move($path,$filename);
            $b->logo=$filename;
        }
        $b->save();
        // dd($b->with('brandimages'));
        $request->session()->flash('success','Brands Created Successfully');
        return Redirect::to('/admin/brands/editmainbrand'.'/'.$b->id);
    }
    public function saveBrand(Request $request)
    {
         $request->validate([
            'name' => 'required',
            'description'=>'required',
            'logo'=>'mimes:jpeg,bmp,png'
        ]);
        $b= Brand::where('id',$request->id)->first();
        $b->name=$request->name;
        $b->description=$request->description;
         $file=$request->file('logo');
        if($file!=null)
        {

            $filename=time() . '.' .$file->getClientOriginalExtension();
            $path='admin-assets/uploads';
            $file->move($path,$filename);
            if($b->logo!=null)
                unlink($path.'/'.$b->logo);
            $b->logo=$filename;
        }
        $b->save();
        // dd($b->description);
        Session::flash('success', 'Brand Edited');

        return Redirect::to('/admin/brands/editmainbrand'.'/'.$b->id);
    }
    public function removeImage($id,Request $request){
        $image = Brandimage::find($id);
        unlink(public_path('admin-assets/uploads'.'/'.$image->image));
        $image->delete();

        Session::flash('success', 'Image deleted.');
        return Redirect::to('admin/brands/editmainbrand'.'/'.$request->brand_id);
    }
     public function addImage(Request $request,$id){
        $_image = ['file'=>''];
        if($request->hasFile('files')){
            $images = $request->file('files');
            
        }
        // foreach($images as $image){
        //     $_image []= $image->getClientOriginalExtension();
        // }
        // dd($_image);
        $count = 0;
        foreach($images as $image){
            $Limage = new Brandimage;
            $filename = time().'l'.$count++ . '.' . $image->getClientOriginalExtension();
            $location = public_path('admin-assets/uploads');
            $image->move($location,$filename);
            $Limage->image = $filename;
            $Limage->brand_id=$id;
            $Limage->save();
        }
        
        $request->session()->flash('success', 'Images Uploaded');
        return Redirect::to('/admin/brands/editmainbrand'.'/'.$id);
    }
    	
}
