<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Quotation;
use App\Contact;
use App\ServiceSupportMessage;
use Auth;

class AdminMenuComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $qno = 0;
        $mno = 0;
        $sno = 0;
        
        if (Auth::user()->roles()->where('title', '=', 'Quotations')->exists())
            $qno = Quotation::where('seen','0')->count();

        if (Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists())
            $mno = Contact::where('seen','0')->count();
        if (Auth::user()->roles()->where('title', '=', 'Service Support Messages')->exists())
            $sno = ServiceSupportMessage::where('seen','0')->count();
        $view->with('qno',$qno)->with('mno',$mno)->with('sno',$sno);
    }
}