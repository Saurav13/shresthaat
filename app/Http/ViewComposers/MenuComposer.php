<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Brand;
use App\Solution;

class MenuComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $brands=Brand::all();
        $solutions=Solution::all();
        $view->with('brands',$brands)->with('solutions',$solutions);
    }
}