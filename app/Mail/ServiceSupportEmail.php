<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\ServiceSupportMessage;

class ServiceSupportEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new service_message instance.
     *
     * @return void
     */
    public $service_message;

    public function __construct(ServiceSupportMessage $service_message)
    {
        $this->service_message = $service_message;
    }

    /**
     * Build the service_message.
     *
     * @return $this
     */
    public function build()
    {
        // $this->setMailConfig();
        return $this->subject($this->service_message->subject)->view('emails.servicesupportemail')->with('service_message',$this->service_message);

    }
}
