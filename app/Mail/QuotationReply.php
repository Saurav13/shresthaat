<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuotationReply extends Mailable
{
    use Queueable, SerializesModels;

    protected $customer;
    protected $content;
    protected $attachment;

    public function __construct($customer,$content, $attachment,$subject)
    {
        $this->customer = $customer;
        $this->content = $content;
        $this->subject = $subject;
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->subject($this->subject)->view('emails.quotationreply')->with('customer',$this->customer)
            ->with('content',$this->content)
            ->attachData($this->attachment, 'quote.pdf', [
                'mime' => 'application/pdf',
            ]);


    }
}
