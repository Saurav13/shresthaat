<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//general routes
Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/brand/{name}','Frontend\ProductController@brand');
Route::get('/products','Frontend\ProductController@index');
Route::get('/products/search','Frontend\ProductController@search');
Route::get('/singleproduct','Frontend\ProductController@singleProduct');

Route::get('/services', 'Frontend\FrontendController@services')->name('services');
Route::post('/sendServiceMessage', 'Frontend\FrontendController@sendServiceMessage')->name('sendServiceMessage');

Route::get('/get-a-quote', 'Frontend\FrontendController@getQuote')->name('getQuote');
Route::post('/sendquote', 'Frontend\FrontendController@sendQuote')->name('sendQuote');
Route::post('/getProducts', 'Frontend\FrontendController@getProducts')->name('getProducts');

Route::get('/contact', 'Frontend\FrontendController@contact')->name('contact');
Route::post('/contactSend', 'Frontend\FrontendController@contactSend')->name('contactSend');

Route::get('/about', 'Frontend\FrontendController@about')->name('about');

Route::get('/gallery/albums','Frontend\FrontendController@albums')->name('gallery.albums');
Route::get('/gallery/album/{slug}', 'Frontend\FrontendController@album')->name('gallery.album');

Route::post('/subscribe','HomeController@subscribe')->name('subscribe');
Route::get('unsubscribe/{token}','HomeController@unsubscribe')->name('unsubscribe');
Route::post('unsubscribe/{token}','HomeController@unsubscribed')->name('unsubscribed');

Route::get('/home', 'HomeController@index')->name('home');


//routes for admin
Route::prefix('admin')->group(function(){
    Auth::routes();

    Route::get('staffs','Admin\StaffController@index')->name('staffs.index');
    Route::post('staffs/update','Admin\StaffController@update')->name('staffs.update');    
    Route::post('staffs/delete/{id}','Admin\StaffController@destroy')->name('staffs.destroy');
    Route::post('verifyPassword','Admin\StaffController@verifyPassword')->name('verifyPassword');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

    Route::resource('services', 'Admin\ServiceController',['except' => ['show']]);

    Route::get('quotations/unseen','Admin\QuotationController@unseenQuotes')->name('quotations.unseen');    
    Route::get('quotations/reply/{id}','Admin\QuotationController@reply')->name('quotations.reply');
    Route::post('quotations/reply/{id}','Admin\QuotationController@replySend')->name('quotations.replySend');
    Route::get('getUnseenQuoteCount','Admin\QuotationController@getUnseenQuoteCount')->name('getUnseenQuoteCount');
    Route::get('getUnseenQuote','Admin\QuotationController@getUnseenQuote')->name('getUnseenQuote');
    Route::resource('quotations', 'Admin\QuotationController',['only' => ['show','destroy','index']]);

    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');

    Route::resource('testimonials', 'Admin\TestimonialController',['only' => ['index','store','edit','update','destroy']]);

    Route::get('contact-us-messages/unseen','Admin\ContactController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactController@getUnseenMsg')->name('getUnseenMsg');

    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);
    
    Route::get('service-support-messages','Admin\ServiceSupportMessageController@addImages1')->name('service-support-messages.index');
    Route::get('service-support-messages/unseen','Admin\ServiceSupportMessageController@unseenMsg')->name('service-support-messages.unseen');    
    Route::get('service-support-messages/reply/{id}','Admin\ServiceSupportMessageController@reply')->name('service-support-messages.reply');
    Route::post('service-support-messages/reply/{id}','Admin\ServiceSupportMessageController@replySend')->name('service-support-messages.replySend');
    Route::resource('service-support-messages', 'Admin\ServiceSupportMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenSSMsgCount','Admin\ServiceSupportMessageController@getUnseenSSMsgCount')->name('getUnseenSSMsgCount');
    Route::get('getUnseenSSMsg','Admin\ServiceSupportMessageController@getUnseenSSMsg')->name('getUnseenSSMsg');

    Route::get('/dashboard','Admin\AdminController@index')->name('admin_dashboard');
    Route::get('/','Admin\AdminController@index');
    
    Route::get('/products/search','ProductController@search');
	Route::post('/products/addbasics','ProductController@addBasics');
	Route::post('/products/editbasics','ProductController@editBasics');
    Route::get('/products/getbrandsandsolutions','ProductController@getBrandsAndSolutions');
	Route::post('/products/addimage/{id}','ProductController@addImage');
    Route::post('/products/removeimage','ProductController@removeImage');
    Route::resource('/products','ProductController');
    Route::post('/products/savefeatures','ProductController@saveFeatures');
    Route::post('/products/savespecification','ProductController@saveSpecification');
    Route::post('/products/savepurpose','ProductController@savePurpose');
    Route::post('/products/savefaqs','ProductController@saveFaqs');
    Route::post('/products/adddriver','ProductController@adddriver');
    Route::post('/products/savedriver','ProductController@savedriver');
    Route::post('/products/removedriver','ProductController@removedriver');
    Route::get('/products/changefeatured/{id}','ProductController@changeFeatured'); 
    Route::get('/products/changestatus/{id}','ProductController@changeStatus'); 
   
	Route::post('/brands/createmainbrand','BrandController@createMainBrand');
    Route::post('/brands/createbrand','BrandController@createBrand');
    Route::get('/brands','BrandController@index')->name('brands.index');
	Route::get('/brands/getbrands','BrandController@getBrands');
	Route::get('/brands/brandsbyid','BrandController@getSingleBrand');
    Route::post('/brands/savebrand','BrandController@saveBrand');
    Route::post('/brands/create','BrandController@addBrand');
	Route::post('/brands/edit','BrandController@editBrand');
	Route::post('/brands/editParent','BrandController@editBrandParent');
	Route::post('/brands/delete','BrandController@deleteBrand');
    Route::get('/brands/editmainbrand/{id}','BrandController@editMainBrand');
    Route::delete('/brands/removeImage/{id}','BrandController@removeImage')->name('admin.brands.removeImage');
    Route::post('/brands/addImage/{id}','BrandController@addImage')->name('admin.brands.addImage');
    Route::post('/brands/haschild','BrandController@hasChild');


	Route::get('/solutions','SolutionController@index')->name('solutions.index');
    Route::get('/solutions/getsolutions','SolutionController@getSolutions');
    Route::get('/solutions/solutionsbyid','SolutionController@getSingleSolution');
    Route::post('/solutions/create','SolutionController@addSolution');
    Route::post('/solutions/edit','SolutionController@editSolution');
    Route::post('/solutions/editParent','SolutionController@editSolutionParent');
    Route::post('/solutions/delete','SolutionController@deleteSolution');
    Route::post('/solutions/createSolution','SolutionController@createSolution');
    Route::post('/solutions/haschild','SolutionController@hasChild');    
});

Route::get('/assets/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response('jpg');
})->name('asset');
Route::get('/assets/{source}/{source2}/{img}/{h}/{w}',function($source,$source2,$img, $h, $w){
   
    return \Image::make(public_path("/".$source."/".$source2."/".$img))->resize($h, $w)->response('jpg');
})->name('asset1');
Route::get('/asset/{source}/{img}/{ext}/{h}/{w}',function($source,$img,$ext, $h, $w){
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('asset2');
