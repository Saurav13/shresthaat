<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'title' => 'Dashboard'
        ]);
        DB::table('roles')->insert([
            'title' => 'Staffs'
        ]);
        DB::table('roles')->insert([
            'title' => 'Brands'
        ]);
        DB::table('roles')->insert([
            'title' => 'Products'
        ]);
        DB::table('roles')->insert([
            'title' => 'Solutions'
        ]);
        DB::table('roles')->insert([
            'title' => 'Services'
        ]);
        DB::table('roles')->insert([
            'title' => 'Quotations'
        ]);
        DB::table('roles')->insert([
            'title' => 'Albums'
        ]);
        DB::table('roles')->insert([
            'title' => 'Newsletter'
        ]);
        DB::table('roles')->insert([
            'title' => 'Testimonials'
        ]);
        DB::table('roles')->insert([
            'title' => 'Contact Us Messages'
        ]);
        DB::table('roles')->insert([
            'title' => 'Settings'
        ]);
        DB::table('roles')->insert([
            'title' => 'Service Support Messages'
        ]);
    }
}
