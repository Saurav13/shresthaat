<?php

use Illuminate\Database\Seeder;

class GeneralServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'id' => 1,
            'name' => 'Provider Name',
            'email' => 'provider@email.com',
            'address' => 'Provider Address',
            'phone' => 'Provider Contact',
            'title' => 'General service title',
            'description' => '<p>General Description</p>'
        ]);
    }
}
