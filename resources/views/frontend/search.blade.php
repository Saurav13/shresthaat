@extends('layouts.product',['title'=>'| Products'])

@section('body')
<section class="pt-5 pt-md-0">
    <div class="container pt-5">
      <div class="row justify-content-center product-cat-search">
        <div class="col-md-12">
          <nav aria-label="breadcrumb" role="navigation">
            @if($filtype=='brand')
            <ol class="breadcrumb breadcrumb--product">
              <li class="breadcrumb-item"><a href="{{URL::to('/brand'.'/'.$breadcrumbs[0]->name)}}">{{$breadcrumbs[0]->name}}</a></li>
              @foreach(array_slice($breadcrumbs,1,sizeof($breadcrumbs)-2) as $b)

              <li class="breadcrumb-item"><a href="{{URL::to('/products?cid='.$b->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$b->name}}</a></li>
              @endforeach

              <li class="breadcrumb-item active" aria-current="page">{{end($breadcrumbs)->name}}</li>
            </ol>
            @else
            <ol class="breadcrumb breadcrumb--product">
              @foreach(array_slice($breadcrumbs,0,sizeof($breadcrumbs)-1) as $b)
              <li class="breadcrumb-item"><a href="{{URL::to('/products?cid='.$b->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$b->name}}</a></li>
              @endforeach

              <li class="breadcrumb-item active" aria-current="page">{{end($breadcrumbs)->name}}</li>
            </ol>
            @endif
          </nav>
        </div>
      </div>
    </div>
  </section>


  <!-- Products -->
  <section id="products" class="section-padding pb-0 pt-1">
    <div class="container">


      <div class="row no-gutters heading-padding">
        <div class="col-lg-8 col-md-7 d-flex align-items-center justify-content-center justify-content-md-start">
          @if($products->count()>0)
          <p class="lead mb-0 result-count d-none d-md-block">Showing Results:</p>
          @endif
        </div>
        <div class="col-md-5 col-lg-4 d-flex justify-content-center justify-content-md-end">
          <form class="mb-0" action="{{URL::to('/products/search')}}" method="GET">
            <div class="form-container">
              <input type="hidden" value="{{json_encode($breadcrumbs)}}" name="breadcrumbs"/>
              <input type="hidden" value="{{json_encode($category)}}" name="category"/>
               <input type="hidden" value="{{$filtype}}" name="filtype"/>
                           
              <input class="form-control form-control-lg" name="key" type="text" placeholder="Search Your Product">
              <i class="fa fa-search"></i>
            </div>
          </form>
        </div>
      </div>



    <div class="row justify-content-start">
      @forelse($products as $p)
      <div class="col-lg-3 col-md-4 col-sm-6">
      <div class="single-product card card--fixed-size p-2 mb-5">
        <a href="{{URL::to('/singleproduct/?id='.$p->id.'&'.'cid='.$category->id.'&'.'filtype='.$filtype.'&bid='.$bid)}}">
          @if($p->productimages->count()>0)
          <img class="card-img-top img-fluid" src="{{asset('admin-assets/uploads'.'/'.$p->productimages->first()->image)}}" alt="Card image cap">
          @else
          <img class="card-img-top img-fluid" src="{{asset('frontend-assets/assets/images/printer.jpg')}}" alt="Card image cap">
          @endif
          <div class="card-body pb-0 px-0">
            <h3 class="card-title text-center text-capitalize">{{$p->name}}</h3>
            <!-- <p class="card-text text-center">{{substr($p->description,0,60)}}....</p> -->
            <a href="javascript:void(0)" class="btn btn-primary btn-block btn--card" id="quoteButton{{$p->id}}" product="{{$p->name}}">Get a Quote</a>

          </div>
        </a>
      </div> <!-- /.card -->
    </div>
    @empty
     <p class="lead mb-0">No results found for key '{{$key}}'</p>
          
    <br><br><br><br><br><br>
      @endforelse
      

   </div> <!-- /.row -->
 </div> <!-- /.container -->
</section> <!-- /#products -->


<!-- Pagination -->
<!-- <section id="pagination" class="pb-5">
  <div class="container">
    <div class="row">
      <div class="col-12 d-flex justify-content-center">
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section> -->





@endsection