@extends('layouts.app',['title'=>'| Get A Quote'])

@section('body')
    <link href="/frontend-assets/assets/css/select2.css" rel="stylesheet" />
    {{--  <link rel="stylesheet" href="/frontend-assets/assets/css/easy-autocomplete.css">  --}}
     <!-- quotation from anywhere  -->
    <section id="quotation-form" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center heading-padding">
                    <h2 class="text-capitalize">Please fill in the following details</h2>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="quotation-form">
                        <form class="clearfix" id="quoteForm" method="POST" action="{{ route('sendQuote') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <p>
                                        <input type="text" name="firstname" placeholder="Enter Your First Name" required>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <input type="text" name="lastname" placeholder="Enter Your Last Name" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>
                                        <input type="email" name="email" placeholder="Enter Your Email" required>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <input type="text" name="phone" placeholder="Enter Your Phone Number" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p>
                                        <textarea name="message" cols="30" rows="10" placeholder="Enter Your Message (Optional)"></textarea>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <p>
                                        <select class="js-example-basic-multiple" multiple="multiple" id="newProduct"  width="100%"></select>
                                    </p>
                                </div>
                                <div class="col-sm-2">
                                    <p>
                                        <input type="number" id="newQuantity" value="1" min="1">
                                    </p>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" id="addProduct" class="btn float-right">Add</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 product-table">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" width="70%">Selected Product</th>
                                                <th scope="col">Image</th>
                                                <th scope="col" width="13%">Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody id="productList">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button id="sendQuote" class="btn float-right"><a>Send</a></button>
                        </form>    
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="/frontend-assets/assets/js/jquery.easy-autocomplete.js"></script>
    <script src="/frontend-assets/assets/js/select2.js"></script>
    <script>
        $(document).ready(function(){
            var allProducts = JSON.parse('<?php echo $products ?>');

            $('#newProduct').select2({
                placeholder: "Search Your Desired Product",
                width: '100%',
                height: '45px',
                data : allProducts,
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatData
            });

            function formatData (data) {
                if(data.data!='')
                    var $result= $(
                        '<span><img src="/admin-assets/uploads/'+data.data+'" style="height:45px;width:100px"/> ' + data.text + '</span>'
                    );
                else
                    var $result= $(
                        '<span>' + data.text + '</span>'
                    );
                return $result;
            }
 
            var products = [];
            $('#addProduct').click(function(){
                var selected = $('#newProduct').select2('data');
                var quantity = parseInt($('#newQuantity').val());

                if(selected.length >0 && quantity >0){
                    selected.forEach(function(p){
                        var pro = {'name':p.text,'code':p.id,'quantity':quantity};
                        var temp = check(pro);
                        if(temp == -1){
                            products.push(pro);
                            var html = '<tr id="P_'+pro.code+'"><td><span class="selected-product"><a class="test" pid="'+pro.code+'"><i class="fa fa-times text-danger" ></i></a> &nbsp;'+ pro.name+'</span></td><td><img  src="/admin-assets/uploads/'+ p.data +'" alt="Card image cap" style="height:45px;width:100px"/></td><td class="quantity"><input type="number"  min="1" class="qnt" pid="'+pro.code+'" value="'+ quantity +'"></td></tr>';
                            $('#productList').append(html);
                        }
                        else{
                            products[temp].quantity += quantity;
                            $('#P_'+ p.id).find('input').val(products[temp].quantity);
                        }
                    });
                    $('#newProduct').val('').trigger('change');
                }
                else{
                    aalert('Error','Please Select Some Products');
                }
            });

            function check(pro){
                var index = -1;
                products.forEach(function(p,i){
                    if(pro.code == p.code)
                        index = i;
                });
                return index;
            }

            $('#productList').on('click',"a.test",function(){
                var id = $(this).attr('pid');
                var del;
                $('#P_'+id).hide('slow', function(){ $('#P_'+id).remove(); });
                products.forEach(function(p,i){
                    if(p.code == id)
                        del = i;
                });
                products.splice(del,1);              
            });

            $('#productList').on('change',"input.qnt",function(){
                var id = $(this).attr('pid');
                var qty = parseInt($(this).val());
                products.forEach(function(p,i){
                    if(p.code == id)
                        products[i].quantity = qty;
                });
            });

            $('#quoteForm').submit(function(e){
                e.preventDefault();
                if(products.length==0){
                    aalert('Error','No Products Selected');
                    return;
                }
                param = "<input name='products' hidden value='" + JSON.stringify(products) + "' />";
                $('#quoteForm').append(param);
                this.submit();
            });
        });

    </script>
@endsection