@extends('layouts.app',['title'=>'| '.$album->name.' '.$album->year])

@section('body')
    <!-- Breadcrumb -->
	<section class="sdp-breadcrumb sdp-breadcrumb--about">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="breadcrumb-left">
						<h3>Know Us</h3>
						<h2>Gallery</h2>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="breadcrumb-right text-center text-md-right">
						<ul class="list-unstyled list-inline">
							<li class="list-inline-item"><a href="{{ route('home') }}">Home</a></li>
							<li class="list-inline-item">gallery</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>



	<section id='gallery' class='section-padding'>
		<div class="row">
			<div class="col-12 d-flex justify-content-center heading-padding pb-3">
				<h1 class="home-latest-product-heading">{{ $album->name }}</h1>
			</div>
		</div>

		<div class="gallery">
            @foreach($album->album_images as $image)
                <a class='gallery-link' href='/gallery/{{$image->image}}'  data-lightbox="roadtrip">
                    <figure class='gallery-image'>
                        <img class="img-fluid" src="{{ route('asset', ['gallery',$image->image,450,450]) }}">
                    </figure>
                </a>
            @endforeach
            
		</div> <!-- /.gallery -->
        @if(count($album->album_images)==0)
            <div class="text-center" style="margin:50px">
                <h3> No Photos Yet</h3>
            </div>
        @endif
	</section>
@endsection