@extends('layouts.product',['title'=>'| Products'])

@section('body')
<section class="pt-5 pt-md-0">
    <div class="container pt-5">
      <div class="row justify-content-center product-cat-search">
        <div class="col-md-11">
          <nav aria-label="breadcrumb" role="navigation">
           @if($filtype=='brand')
            <ol class="breadcrumb breadcrumb--product">
               <li class="breadcrumb-item"><a href="{{URL::to('/brand'.'/'.$breadcrumbs[0]->name)}}">{{$breadcrumbs[0]->name}}</a></li>
              @foreach(array_slice($breadcrumbs,1,sizeof($breadcrumbs)-1) as $b)
              <li class="breadcrumb-item"><a href="{{URL::to('/products?cid='.$b->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$b->name}}</a></li>
              @endforeach

              <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>
            </ol>
            @else
            <ol class="breadcrumb breadcrumb--product">
              @foreach(array_slice($breadcrumbs,0,sizeof($breadcrumbs)-1) as $b)
              <li class="breadcrumb-item"><a href="{{URL::to('/products?cid='.$b->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$b->name}}</a></li>
              @endforeach

              <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>

            </ol>
            @endif
          </nav>
        </div>
      </div>
    </div>
  </section>

  <!-- Single Product -->
  <section id="single-product" class="section-padding pt-4">
    <div class="container">
      <div class="row">
        <div class="col-md-2 product-thumbnail-imgs mb-2 d-flex justify-content-around flex-md-column align-items-md-end justify-content-md-start">
         @foreach($product->productimages->slice(1) as $p)
          <a href="{{asset('admin-assets/uploads'.'/'.$p->image)}}" data-lightbox="roadtrip">
            <img src="{{asset('admin-assets/uploads'.'/'.$p->image)}}" class="img-fluid product-thumbnail-img" alt="single-prduct">
          </a>
          @endforeach
         
        </div>
        @if($product->productimages->count()>0)
        <div class="col-md-4 product-lg-img mb-2">
          <a href="{{asset('admin-assets/uploads'.'/'.$product->productimages->first()->image)}}" data-lightbox="roadtrip">
            <img src="{{asset('admin-assets/uploads'.'/'.$product->productimages->first()->image)}}" class="img-fluid" alt="single-prduct">
          </a>
        </div>
        @else
        <div class="col-md-4 product-lg-img mb-2">
          <a href="assets/images/800x800_MDB-BOX_RUBIX.png" data-lightbox="roadtrip">
            <img src="assets/images/800x800_MDB-BOX_RUBIX.png" class="img-fluid" alt="single-prduct">
          </a>
        </div>
        @endif
        <div class="col-lg-5 col-md-6 product-details mb-2">
          <div class="card border-0">
           <div class="card-body py-0 px-0">
             <h3 class="card-title text-capitalize">{{$product->name}}<span><i>({{$brandname}})</i></span></h3>
              
              <br>
              @if(strlen($product->description)>480)
              <div id="less">
             <p>
              {{substr($product->description,0,480)}}...<a href="javascript:void(0);" id="spviewmore"><span><i>view more</i></span></a>
             </p>
           </div>
           <div id="more" hidden>
             <p>
              {{$product->description}}<a href="javascript:void(0);" id="spviewless"><span><i> view less</i></span></a>
             </p>
           </div>
           @else
              <p>
              {{$product->description}}
             </p>
           @endif
             <a href="javascript:void(0);" class="btn btn-primary btn--card" id="quoteButton{{$product->id}}" product="{{$product->name}}">Get a Quote</a>
            <a href="{{asset('admin-assets/uploads'.'/'.$product->brochure)}}" download class="btn btn-primary btn--card" >Brochure</a>
           
           </div>
         </a>
       </div> <!-- /.card -->
     </div>
   </div>
 </div>
</section>


<!-- Product Tabs -->
<section id="product-features" class="section-padding pt-4">
  <div class="container">

    <div class="row justify-content-lg-center">

      <div class="col-12 col-lg-11 tabs tabs-single-product">

        <ul class="nav nav-tabs" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-specification-tab" data-toggle="pill" href="#pills-specification" role="tab" aria-controls="pills-specification" aria-selected="true">Specification</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" id="pills-features-tab" data-toggle="pill" href="#pills-features" role="tab" aria-controls="pills-features" aria-selected="true">Features</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" id="pills-faq-tab" data-toggle="pill" href="#pills-faq" role="tab" aria-controls="pills-faq" aria-selected="false">FAQ</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" id="pills-download-tab" data-toggle="pill" href="#pills-download" role="tab" aria-controls="pills-download" aria-selected="false">Download</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" id="pills-purpose-tab" data-toggle="pill" href="#pills-purpose" role="tab" aria-controls="pills-purpose" aria-selected="false">Purpose</a>
          </li>
        </ul>

        <!-- Tab contents -->
        <div class="tab-content border border-top-0 p-3" id="pills-tabContent">

          <!-- specification -->
          <div class="tab-pane fade show active" id="pills-specification" role="tabpanel" aria-labelledby="pills-specification-tab">
           @if($product->specification!="")
           {!!$product->specification!!}
           @else
              <p>No specification available currently.</p>
           @endif

          </div><!-- specification -->

          <!-- features -->
          <div class="tab-pane fade show" id="pills-features" role="tabpanel" aria-labelledby="pills-features-tab">
           @if($product->features->count()>0)
            @foreach($product->features as $f)
          
            <div class="features-details mb-4" style="text-align:justify;font-size:15px;">
              <h5 class="features-title">{{$f->title}}</h5>
              <p class="features-text" align="justified">
              {{$f->description}}
              </p>
              
            </div>
          @endforeach
           @else
              <p>No features available currently.</p>
              @endif

         </div>

          <!-- FAQ -->
          <div class="tab-pane fade" id="pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">

            <div id="accordion" role="tablist">
             @if($product->faqs->count()>0)
              @foreach($product->faqs as $f)
              <div class="card">
                <a data-toggle="collapse" href="#collapse{{$f->id}}" aria-expanded="false" aria-controls="collapse{{$f->id}}">
                  <div class="card-header" role="tab" id="heading{{$f->id}}">
                    <h6 class="mb-0">{{$f->question}}</h6>
                  </div>
                </a>

                <div id="collapse{{$f->id}}" class="collapse show" role="tabpanel" aria-labelledby="heading{{$f->id}}" data-parent="#accordion">
                  <div class="card-body">
                    {{$f->answer}}
                    </div>
                </div>
              </div> <!-- /.card -->
              @endforeach
              @else
              <p>No FAQS available currently.</p>
              @endif
              
            </div> <!-- /.accordion -->

          </div> <!-- faq -->


          <!-- download -->
          <div class="tab-pane fade show" id="pills-download" role="tabpanel" aria-labelledby="pills-download-tab">
            @if($product->drivers->count()>0)
               
            <table class="table table-bordered table-hover">
              <thead>
                <tr class="text-center">
                  <th scope="col">Driver</th>
                  <th scope="col">Downloads</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($product->drivers as $d)
                  <tr>
                    <td>{{$d->title}}</td>
                    <td><a href="{{asset('admin-assets/uploads'.'/'.$d->file)}}" download><i class="fa fa-download" aria-hidden="true"></i> &nbsp; download</a></td>
                  </tr>
                  @endforeach
             
                
              </tbody>
            </table>
             @else
                  <p>No downloads available currently.</p>
                @endif
          </div> <!-- download -->

          <!-- Purpose -->
          <div class="tab-pane fade" id="pills-purpose" role="tabpanel" aria-labelledby="pills-purpose-tab">
            <div class="purpose-details mb-4">
              <p class="purpose-text">
             @if($product->purpose!="")
             {!! $product->purpose !!}
              </p>
               @else
              <p>Not available currently.</p>
           @endif

            </div>

         
          </div> <!-- Purpose -->

        </div> <!-- /.tab-content -->

      </div> <!-- /.tabs -->

    </div> <!-- /.row -->

  </div> <!-- /.container -->
</section>






@endsection
@section('js')
<script>
$(document).ready(function(){
  $('#spviewmore').click(function(){
    // console.log('lkala')
    $('#less').attr('hidden','true');
    $('#more').removeAttr('hidden');
  })
$('#spviewless').click(function(){
    $('#more').attr('hidden','true');
    $('#less').removeAttr('hidden');
  })  
})
</script>
@endsection