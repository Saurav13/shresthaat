@extends('layouts.app',['title'=>'| Our Services'])

@section('body')
     <!-- Breadcrumb -->
    <section class="sdp-breadcrumb sdp-breadcrumb--about">
        <div class="container">
        <div class="row">
            <div class="col-sm-6">
            <div class="breadcrumb-left">
                <h3>Feel Free</h3>
                <h2>Our Services</h2>
            </div>
            </div>
            <div class="col-sm-6">
            <div class="breadcrumb-right text-center text-md-right">
                <ul class="list-unstyled list-inline">
                <li class="list-inline-item"><a href="{{ route('home') }}">home</a></li>
                <li class="list-inline-item">services</li>
                </ul>
            </div>
            </div>
        </div>
        </div>
    </section>
    <div style="text-align:right;margin:20px;margin-bottom:-20px;">
        {{--  This special button is no longer needed  --}}
        <button class="btn btn-sm" onclick="serviceMessage(0)" style="background: #ed1c24;color: #fff;">Ask for Support</button>
    </div>
    <section id="about-section" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="about-section-left col-margin-bottom">
                        <img src="{{ route('asset2', ['images','avatar.png','png',240,240]) }}" alt="abt image" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 d-flex align-items-center">
                    <div class="about-section-right">
                        <h3 class="service-title">Head Office</h3>
                        <p class="service-text"><strong>Contact Name :</strong>&nbsp; Rajesh Shrestha</p>
                        <p class="service-text"><strong>Contact Email :</strong>&nbsp; rajesh@rajehs.com</p>
                        <p class="service-text"><strong>Address :</strong>&nbsp; Hattisar</p>
                        <p class="service-text"><strong>Contact No :</strong>&nbsp; 98089898887</p>
                        <p class="service-text">
                            <strong>Description :</strong>&nbsp;
                            {{--  @if(strlen(strip_tags($service->description))>150)
                            <div id="notalltext{{$service->id}}">
                                <span>{!! substr(strip_tags($service->description),0,150) !!}<text id="dotmore"> ...&nbsp;</text><a href="javascript:void(0)" class="service-read-more" id="more{{$service->id}}"> View More</a></span>
                            </div>
                            <div id="alltext{{$service->id}}" hidden>
                                <span >{!! $service->description !!} <a href="javascript:void(0)" id="less{{$service->id}}" class="service-read-more">Hide</a></span>
                            </div>
                            @else
                            <div >
                                <span >{!! $service->description !!}</span>
                            </div>
                            @endif  --}}
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with 
                            desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                        </p>                             

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="services" class="section-padding">
        <div class="container">
                <h3 class="text-center">Other Services</h3>
            <div class="row justify-content-center">
              
                @foreach($services as $service)
                    <div class="col-lg-10 py-3 mb-5 service">
                        <div class="row">
                            <div class="col-sm-3 service-image" style="text-align:center">
                                
                                @if($service->photo)
                                    <img src="{{ route('asset', ['service_images',$service->photo,240,240]) }}" height="255" class="img-fluid" alt="service person">
                                @else
                                    <img src="{{ route('asset2', ['images','avatar.png','png',240,240]) }}" height="255" class="img-fluid" alt="service person"/>
                                @endif
                                <div style=" text-align:center">
                                    <a href="javascript:void(0)" class="btn btn-sm" onclick="serviceMessage({{ $service->id }})" style="margin-top:10px; background: #ed1c24;color: #fff;border-radius: 25px;">Ask for Support</a>
                                </div>
                            </div>
                            <div class="col-sm-9 service-details">
                                <h3 class="service-title">{{ $service->title }}</h3>
                                <p class="service-text"><strong>Contact Name :</strong>&nbsp; {{ $service->name }}</p>
                                <p class="service-text"><strong>Contact Email :</strong>&nbsp; {{ $service->email }}</p>
                                <p class="service-text"><strong>Address :</strong>&nbsp; {{ $service->address }}</p>
                                <p class="service-text"><strong>Contact No :</strong>&nbsp; {{ $service->phone }}</p>
                                <p class="service-text">
                                    <strong>Description :</strong>&nbsp;
                                    @if(strlen(strip_tags($service->description))>150)
                                        <div id="notalltext{{$service->id}}">
                                            <span>{!! substr(strip_tags($service->description),0,150) !!}<text id="dotmore"> ...&nbsp;</text><a href="javascript:void(0)" class="service-read-more" id="more{{$service->id}}"> View More</a></span>
                                        </div>
                                        <div id="alltext{{$service->id}}" hidden>
                                            <span >{!! $service->description !!} <a href="javascript:void(0)" id="less{{$service->id}}" class="service-read-more">Hide</a></span>
                                        </div>
                                    @else
                                        <div >
                                            <span >{!! $service->description !!}</span>
                                        </div>
                                    @endif
                                </p>                             
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
  

    <!-- Pagination -->
    <section id="pagination" class="pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <nav aria-label="Page navigation example">
                        {{ $services->links() }}
                        
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <!-- MODAL Services  -->
    <section id="modal">
        <div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="margin-top:-40px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="serviceModalLabel">Ask For Service Support</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <form id="serviceModalForm" method="POST" action="{{ route('sendServiceMessage') }}">
                        <div class="modal-body">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <input type="text" name="name" placeholder="Enter Your Name" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <input type="email" name="email" placeholder="Enter Your Email" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <input type="text" name="phone" placeholder="Enter Your Phone Number" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <input type="text" name="subject" placeholder="Your Subject" required>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <textarea placeholder="Message..." name="message" required></textarea>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn--modal" id="buttonValue">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('[id *= more]').click(function(){
                var id=$(this).attr('id').slice(4);
                $('#alltext'+id).removeAttr('hidden');
                $('#notalltext'+id).attr('hidden','true');
            });
            $('[id *= less]').click(function(){
                var id=$(this).attr('id').slice(4);
                $('#alltext'+id).attr('hidden','true');
                $('#notalltext'+id).removeAttr('hidden');
            });
        });

        function serviceMessage(id){
            $("input[name='id']").val(id);
            $('#serviceModal').modal('show');
        }

        $('#serviceModalForm').submit(function(e){
            e.preventDefault();

            $('#buttonValue').text('Sending...');
            $.ajax(
            {
                url: "{{ route('sendServiceMessage') }}",
                type: "post",
                data:{'_token':"{{csrf_token()}}", 'id':$("input[name='id']").val(), 'name':$("input[name='name']").val(), 'email':$("input[name='email']").val(), 'phone':$("input[name='phone']").val(),'subject':$("input[name='subject']").val(),'message':$("[name='message']").val()}
            })
            .done(function(response)
            {
                $('#buttonValue').text('Send');
                if(response.errors){
                    var html = '<div class="alert alert-danger"><strong>Errors:</strong><ul>';
                    for(error in response.errors){
                        html += '<li>'+ response.errors[error] +'</li>';
                    }
                    html += '</ul></div>';
                    aalert('Error', html);
                }
                else{
                    $("[name='message']").val('');
                    $('#serviceModal').modal('hide');
                    aalert('Thank You', "We'll soon respond to you");
                }
            })
            .fail(function(){
                $('#buttonValue').text('Send');
                $('#serviceModal').modal('hide');
                aalert('Error', "Please try again");
            });
            
        });
        
    </script>
    
@endsection