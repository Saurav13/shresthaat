@extends('layouts.product',['title'=>'| Products'])

@section('body')
 <section id="homeslider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php $c=0 ?>
      @foreach($brand->brandimages as $b)
      <?php $c=$c+1 ?>
      <li data-target="#homeslider" data-slide-to="{{$c}}" class="{{$c==1?'active':''}}"></li>
      @endforeach
    </ol>
    <div class="carousel-inner">
    	@if($brand->brandimages->count()>0)
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('admin-assets/uploads'.'/'.$brand->brandimages[0]->image)}}" alt="First slide">
      </div>
      @foreach($brand->brandimages->slice(1) as $b)
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('admin-assets/uploads'.'/'.$b->image)}}" alt="Second slide">
      </div>
      @endforeach
      @else
       <div class="carousel-item">
        <img class="d-block w-100" src="frontend-assets/sassets/images/homeslider4.jpg" alt="Third slide">
      </div>
      @endif
     
    </div>
    <a class="carousel-control-prev" href="#homeslider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#homeslider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </section>


  <!-- about -->
  <section id="brand" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center heading-padding">
          <h1 class="home-about-heading">{{$brand->name}}</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="brand-description">
            <!-- <h2>More Than 1000 Financial Planners</h2> -->
           {{$brand->description}}
          </div>
        </div>
      </div>
    </div>
  </section>


  



@endsection