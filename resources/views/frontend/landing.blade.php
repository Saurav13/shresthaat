@extends('layouts.app',['title'=>'| Home'])

@section('body')


  <!-- Home page slider -->
  <section id="homeslider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      @foreach($landing_images as $image)
        <li data-target="#homeslider" data-slide-to="{{ $loop->iteration -1 }}" class="{{ $loop->iteration==1?'active':'' }}"></li>
      @endforeach
    </ol>
    <div class="carousel-inner">
      @foreach($landing_images as $image)
        <div class="carousel-item {{ $loop->iteration==1?'active':'' }}">
          <img class="d-block w-100" src="/landing_images/{{ $image->image }}" alt="{{ $loop->iteration }} slide">
        </div>
      @endforeach
    </div>
    <a class="carousel-control-prev" href="#homeslider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#homeslider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </section>


  <!-- about -->
  <section id="home-about-section" class="section-padding mb-4">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center heading-padding">
          <h1 class="home-about-heading">About Us</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-6 d-none d-sm-block ">
          <div class="about-section-left col-margin-bottom">
            <img src="{{ route('asset', ['landing_images',$about->photo,350,350]) }}" alt="abt image" class="img-fluid">
          </div>
        </div>
        <div class="col-lg-8 col-md-6 d-flex align-items-center">
          <div class="about-section-right">
            {!! $about->content !!}
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- Latest Products -->
  @if(count($featured)>0)
  <section id="home-latest-products" class="section-padding mb-5">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center heading-padding">
          <h1 class="home-latest-product-heading">Featured Products</h1>
        </div>
      </div>
    </div>

    <div class="container px-0">
      <div class="row justify-content-center">
        @foreach($featured as $p)
          <div class="col-md-3 col-sm-6 product">
            <a href="{{URL::to('/singleproduct/?id='.$p->id.'&'.'cid='.$p->brand_id.'&'.'filtype=brand'.'&bid=')}}">
              <div class="product">
                <!-- <div class="product-overlay"></div> -->
                <div class="product-image">
                  @if($p->productimages->count()>0)
                    <img class="card-img-top" src="{{asset('admin-assets/uploads'.'/'.$p->productimages->first()->image)}}" alt="{{ $p->name }}">
                  @else
                    <img class="card-img-top" src="frontend-assets/assets/images/printer5.png" alt="{{ $p->name }}">
                  @endif
                </div>
                <h3 class="product-title text-center"> {{ $p->name }}</h3>
              </div>
              <div class="product-btn-quote">
                <a class="btn btn-md" style="background: #ed1c24;color: #fff;" href="javascript:void(0)" id="quoteButton{{$p->id}}" product="{{$p->name}}">Get a quote</a>
              </div>
            </a>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif

  <!-- brand slider -->
  <section id="brands" class="section-padding my-5">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-center heading-padding">
          <h1 class="home-brand-heading">Our Brands</h1>
        </div>
      </div>
      <div id="owl-carousel-logo" class="owl-carousel">
        @foreach($brands as $b)
        <img src="/admin-assets/uploads/{{$b->logo}}" class="img-fluid px-5" alt="">
       @endforeach
        </div>
    </div>
  </section>


  <!-- Testimonial -->
 <section id="testimonial" class="section-padding">
 @if($testimonials->count()>0)

   <div class="container">
     <div class="row">
       <div class="col-12 d-flex justify-content-center">
         <h1 class="text-capitalize text-center">Happy Customers</h1>
       </div>
     </div>
     <div class="row">
       <div class='content'>
        <div class='slider single-item'>
          <div id="owl-carousel-testimonial" class="owl-carousel">
          @foreach($testimonials as $testimonial)
            <div class='quote-container'>
              <div class='portrait octogon'>
                @if($testimonial->photo)
                  <img alt='{{$testimonial->name}}' src="{{ route('asset', ['landing_images',$testimonial->photo,140,140]) }}">
                @else
                  <img src="{{ route('asset2', ['images','avatar.png','png',140,140]) }}" alt="{{ $testimonial->name }}"/>
                @endif
              </div>
              <div class='quote'>
                <blockquote>
                  <p>{{ $testimonial->content }}</p><cite>
                    <span><strong>{{$testimonial->name}}</strong></span>
                    <br>
                    {{ $testimonial->profession }}
                  </cite>
                </blockquote>
              </div>
            </div>
            @endforeach

           
          </div>

        </div>
      </div>

      <svg>
        <defs>
          <clipPath clipPathUnits='objectBoundingBox' id='octogon'>
            <polygon points='0.50001 0.00000, 0.61887 0.06700, 0.75011 0.06721, 0.81942 0.18444, 0.93300 0.25001, 0.93441 0.38641, 1.00000 0.49999, 0.93300 0.61887, 0.93300 0.75002, 0.81556 0.81944, 0.74999 0.93302, 0.61357 0.93444, 0.50001 1.00000, 0.38118 0.93302, 0.24998 0.93302, 0.18056 0.81556, 0.06700 0.74899, 0.06559 0.61359, 0.00000 0.49999, 0.06700 0.38111, 0.06700 0.25001, 0.18440 0.18058, 0.25043 0.06700, 0.38641 0.06559, 0.50001 0.00000'></polygon>
          </clipPath>
        </defs>
      </svg>

    </div><!-- /.row -->
  </div> <!-- /.container -->
@endif

</section> <!-- /.testimonial -->


<section id="modal">
  <div class="modal fade" id="autopopup" tabindex="-1" role="dialog" aria-labelledby="autopopupLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="autopopupLabel">Our Latest Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-5">
              <img src="/frontend-assets/assets/images/printer2.jpg" alt="" class="img-fluid">
            </div>
            <div class="col-md-7">
              <div class="modal-description">
                <h5 class="text-capitalize">Printer one</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis illum dolores laborum voluptatibus voluptatum tempore sequi repellendus est quidem cumque, minus saepe ut magnam itaque, animi reprehenderit iste, dolorem libero.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn--modal" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection