@extends('layouts.app',['title'=>'| Unsubscribe'])

@section('body')
    <!-- Breadcrumb -->
    <section class="sdp-breadcrumb sdp-breadcrumb--about">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="breadcrumb-left">
                        <h3></h3>
                        <h2>Newsletter</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="breadcrumb-right text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="list-inline-item">Unsubscribe</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

  <!-- about -->
    <section id="about-section" class="section-padding">
        <div class="container">
            <form style="text-align:center" method="POST" action="{{route('unsubscribed',$token)}}">
                {{ csrf_field() }}
                <h2 class="">Unsubscribe?</h2>
                <p class="text-capitalize">You will no longer be updated.</p>

                <br>
                <div id="content">
                    <a href="/" style="color: white;" class="btn btn-md btn--quote">Cancel</a>
                    <button class="btn btn-md" style="cursor:pointer;" type="submit">Unsubscribe</button>
                </div>
            </form>
        </div>
    </section>

    <div class="pt-3 pb-5"></div>
@endsection
