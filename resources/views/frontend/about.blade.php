@extends('layouts.app',['title'=>'| About Us'])

@section('body')
    <!-- Breadcrumb -->
    <section class="sdp-breadcrumb sdp-breadcrumb--about">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="breadcrumb-left">
                        <h3>Who we are</h3>
                        <h2>About Our Company</h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="breadcrumb-right text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="{{ route('home') }}">home</a></li>
                        <li class="list-inline-item">about</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

  <!-- about -->
    <section id="about-section" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="about-section-left col-margin-bottom">
                        <img src="{{ route('asset', ['landing_images',$about->photo,350,350]) }}" alt="abt image" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 d-flex align-items-center">
                    <div class="about-section-right">
                        {!! $about->content !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="parallax parallax--about">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-sm-8 text-center">
                    <h2>Word of Wise</h2>
                    <p>When the guidelines were agreed, it was clear that this was more than rhetoric. EU governments have essentially committed</p>
                    <a href="#" data-toggle="modal" data-target="#subscribe" class="btn btn-md btn--quote" >Subscribe</a>
                </div>
            </div>
        </div>
    </section>

    <div class="pt-3 pb-5"></div>

    <section class="container section-padding"> 
        <div id="aboutslider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach($images as $image)
                    <div class="carousel-item {{ $loop->iteration==1?'active':'' }}">
                        <img class="d-block w-100" src="{{ route('asset', ['gallery',$image->image,1140,445]) }}" alt="{{ $loop->iteration }} slide">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#aboutslider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#aboutslider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- /#aboutslider -->
    </section><!-- /.container -->

    <section id="about-section-two" class="section-padding">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-divider about-feature">
                    <i class="fa fa-bar-chart fa--about" aria-hidden="true"></i>
                    <h3>Increase In Revenue</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.</p>
                </div>
                <div class="col-md-4 col-divider about-feature">
                    <i class="fa fa-rss fa--about" aria-hidden="true"></i>
                    <h3>Positive Feedback</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.</p>
                </div>
                <div class="col-md-4 col-divider about-feature">
                    <i class="fa fa-comment-o fa--about" aria-hidden="true"></i>
                    <h3>More Conversion</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste architecto sed.</p>
                </div>
            </div>
        </div>
    </section>
@endsection
