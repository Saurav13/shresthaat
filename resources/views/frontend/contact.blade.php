@extends('layouts.app',['title'=>'| Contact Us'])

@section('body')
    <!-- Breadcrumb -->
  <section class="sdp-breadcrumb sdp-breadcrumb--about">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="breadcrumb-left">
            <h3>Get In Touch</h3>
            <h2>Contact Us</h2>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="breadcrumb-right text-center text-md-right">
            <ul class="list-unstyled list-inline">
              <li class="list-inline-item"><a href="{{ route('home') }}">home</a></li>
              <li class="list-inline-item">contact</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- contact form -->
  <section id="contact" class="section-padding">
    <div class="container">
      <div class="row section-padding">
        <div class="col-sm-10">
          <h2 class="text-capitalize">Contact us any time</h2>
          <p class="text-capitalize">Please send us your feedback on our products and services, including comments, complaints and suggestions. If you are not able to solve the issue after reading FAQs, please send us a message.</p>
        </div>
      </div>

      <div class="contact-form">
        <div class="row">
          <div class="col-lg-4">
            <div class="contact-form-left">
              <div class="single-contact-left">
                <div class="contact-icon ">
                  <i class="fa fa-phone text-center"></i>
                </div>
                <div class="contact-text">
                  <h4 class="text-capitalize">Phone</h4>
                  <?php $phones = explode (',' ,$contact->phone) ?>
                  @foreach($phones as $p)
                    <p>{{ $p }}</p>
                  @endforeach
                </div>
              </div>
              <div class="single-contact-left">
                <div class="contact-icon">
                  <i class="fa fa-envelope text-center"></i>
                </div>
                <div class="contact-text">
                  <h4 class="text-capitalize">Email</h4>
                  <?php $emails = explode (',' ,$contact->email) ?>
                  @foreach($emails as $p)
                    <p>{{ $p }}</p>
                  @endforeach
                </div>
              </div>
              <div class="single-contact-left">
                <div class="contact-icon">
                  <i class="fa fa-home text-center"></i>
                </div>
                <div class="contact-text">
                  <h4 class="text-capitalize">Address</h4>
                  <span class="text-capitalize">{{ $contact->address }}<br>Nepal</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="contact-form-right">
              <form method="POST" action="{{ route('contactSend') }}">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-6">
                    <p>
                      <input type="text" name="name" placeholder="Enter Your Name" required>
                    </p>
                  </div>
                  <div class="col-md-6">
                    <p>
                      <input type="email" name="email" placeholder="Enter Your Email" required>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <p>
                      <input type="text" name="subject" placeholder="Your Subject" required>
                    </p>
                  </div>
                  <div class="col-md-6">
                    <p>
                      <input type="tel" name="phone" placeholder="Your Phone" required>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <p>
                      <textarea placeholder="Message..." name="message" required></textarea>
                    </p>
                    <p class="mb-0">
                      <button type="submit" class="btn btn-lg">Send</button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- MAP -->
  <section id="google-map">
    <div class="row no-gutters">
      <div class="col-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1766.0882298397796!2d85.32199260782865!3d27.71183749416809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1905e763c737%3A0x4e567ea5b82b6cc0!2sShrestha+Digital+Print!5e0!3m2!1sen!2snp!4v1511875112476" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </section>

@endsection