@extends('layouts.app',['title'=>'| Gallery'])

@section('body')
    <!-- Breadcrumb -->
	<section class="sdp-breadcrumb sdp-breadcrumb--about">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="breadcrumb-left">
						<h3>Know Us</h3>
						<h2>Gallery</h2>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="breadcrumb-right text-center text-md-right">
						<ul class="list-unstyled list-inline">
							<li class="list-inline-item"><a href="{{ route('home') }}">Home</a></li>
							<li class="list-inline-item">gallery</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>



	<section id='gallery' class='section-padding'>
		<div class="row">
			<div class="col-12 d-flex justify-content-center heading-padding pb-3">
				<h1 class="home-latest-product-heading mb-5">Our Gallery</h1>
			</div>
		</div>

		<div class="gallery--album">
			<div class="container">
                @foreach($year_albums as $year=>$albums)
                    <div class="row mb-4">
                        <div class="col-12 year">Year: {{ $year }}</div>
                        @foreach($albums as $album)
                            <div class="col-md-3 col-sm-4 mb-4">
                                <a class='gallery-link' href="{{ route('gallery.album',$album->slug) }}">
                                    <figure class='gallery-image'>
                                        <img class="img-fluid" src="/gallery/{{$album->album_images->first()->image }}">

                                    </figure>
                                    <figcaption>{{ $album->name }}</figcaption>
                                </a>
                            </div>
                        @endforeach
                    </div> <!-- /.row -->
                @endforeach
                
			</div><!-- /.container -->
		</div> <!-- /.gallery-album -->
        @if(count($year_albums)==0)
            <div class="text-center" style="margin:50px">
                <h3> No Albums Yet</h3>
            </div>
        @endif
	</section>
@endsection