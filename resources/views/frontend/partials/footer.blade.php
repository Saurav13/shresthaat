


 @if(Request::segment(1)=='' or Request::segment(1)=='home')
 <section class="cta" style="margin-top: -190px;">
  @else
 <section class="cta">
 @endif 
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-9 d-flex justify-content-center justify-content-md-start">
          <div class="cta-left text-center">
            <h3 class="cta-quote">We offer best in class service for your needs.</h3>
          </div>
        </div>
        <div class="col-md-3">
          <div class="cta-right text-center text-md-right">
            <a href="{{ route('getQuote') }}" class="btn btn-md btn--cta">Get a quote</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- MODAL Success Message  -->

   <!-- footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 text-center text-lg-left">
          <ul class="list-inline mb-0 py-4">
            <li class="list-inline-item"><a href="{{ route('home') }}">home</a></li>
            <li class="list-inline-item"><a href="{{ route('about') }}">About</a></li>
            <li class="list-inline-item"><a href="{{ route('home') }}">Product</a></li>
            <li class="list-inline-item"><a href="{{ route('services') }}">Service</a></li>
            <li class="list-inline-item"><a href="{{ route('contact') }}">Contact</a></li>
          </ul>
        </div>
        <div class="col-lg-4 text-center text-lg-right">
          <p class="mb-0 py-4">Copyright &copy; 2017</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- MODAL Single Quote  -->
  <section id="modal">
    <div class="modal fade" id="getQuote" tabindex="-1" role="dialog" aria-labelledby="getQuoteLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="margin-top:-55px">
        <div class="modal-content">
          <div class="modal-header">Ask for Quotation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="clearfix" id="quoteModalForm" method="POST" action="{{ route('sendQuote') }}">
              {{ csrf_field() }}
              <input type="text" value="" id="newIdModal" hidden/>
              <div class="row">
                <div class="col-md-6">
                    <p>
                        <input id="QFName" type="text" name="firstname" placeholder="Enter Your First Name" required>
                    </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <input id="QLName" type="text" name="lastname" placeholder="Enter Your Last Name" required>
                    </p>
                </div>
              </div>
              <p>
                  <input id="Qemail" type="email" name="email" placeholder="Enter Your Email" required>
              </p>
              <p>
                  <input id="Qphone" type="text" name="phone" placeholder="Enter Your Phone Number" required>
              </p>
                  
              <div class="row">
                <div class="col-12">
                    <p>
                        <textarea name="message" cols="30" rows="10" placeholder="Enter Your Message (Optional)"></textarea>
                    </p>
                </div>
              </div>
              <p>Quantity:</p>
              <div class="row">
                <div class="col-sm-9">
                  <p>
                    <input type="text"  id="newProductModal" readonly>
                  </p>
                </div>
                <div class="col-sm-3">
                  <p>
                    <input type="number"  id="newQuantityModal" value="1" min="1">
                  </p>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn--modal">Send</button>
              </div>
            </form>
          </div>
          
        </div>
      </div>
    </div>
  </section>

  <!-- MODAL Success Message  -->
  <section id="modal">
      <div class="modal fade" id="alertPopup" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="alerttitle"></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <p id="alertmsg">
                      </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn--modal" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section id="modal">
    <div class="modal fade" id="subscribe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Subscribe to our Newsletter</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form class="clearfix" method="POST" action="{{ route('subscribe') }}">                
                <div class="modal-body">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-12">
                        <p>
                            <input id="suubscribeEmail" type="email" name="subemail" placeholder="Enter Your Email Address" required>
                        </p>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn--modal">Subscribe</button>
                </div>
              </form>
            </div>
        </div>
    </div>
  </section>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.smartmenus/1.1.0/jquery.smartmenus.min.js"></script>

<!-- Scroll Reveal -->
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

<!-- Owl Carousel -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<!-- lightbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>

<!-- Custom JS -->
<script src="/frontend-assets/assets/js/bundle.min.js" charset="utf-8"></script>
  <script>
      function aalert(title,msg){
        $('#alerttitle').text(title);
        $('#alertmsg').html(msg);              
        $('#alertPopup').modal('show');
      }

      $('[id*=quoteButton]').click(function(){
        var id = $(this).attr('id').slice(11);
        var product_name = $(this).attr('product');
        $('#newProductModal').val(product_name);
        $('#newIdModal').val(id);
        $('#getQuote').modal('show');
      });

      $('#quoteModalForm').submit(function(e){
        e.preventDefault();
        var product = [{'code':$('#newIdModal').val(),'name':$('#newProductModal').val(),'quantity':$('#newQuantityModal').val()}];
        
        $.ajax(
          {
              url: "{{ route('sendQuote') }}",
              type: "post",
              data:{'_token':"{{csrf_token()}}", 'products':JSON.stringify(product), 'firstname':$("input[name='firstname']").val() ,'lastname':$("input[name='lastname']").val(),'email':$("input[name='email']").val(),'phone':$("input[name='phone']").val(),'message':$("[name='message']").val()}
          })
          .done(function(response)
          {
            if(response.errors){
              var html = '<div class="alert alert-danger"><strong>Errors:</strong><ul>';
              for(error in response.errors){
                  html += '<li>'+ response.errors[error] +'</li>';
              }
              html += '</ul></div>';
              aalert('Error', html);
            }
            else{
              $("[name='message']").val('');
              $('#getQuote').modal('hide');
              aalert('Thank You', "We'll soon respond to your quotation");
            }  
          })
          .fail(function(){
            $('#getQuote').modal('hide');
            aalert('Error', "Please try again");
          });
        
      });

      $(document).on('show.bs.modal', '.modal', function () {
          var zIndex = 1040 + (10 * $('.modal:visible').length);
          $(this).css('z-index', zIndex);
          setTimeout(function() {
              $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
          }, 0);
      });
  </script>
  @if(Session::has('success'))
    <script>
      aalert('Thank You',"{!!Session::get('success')!!}");
    </script>
  @endif
  @if(Session::has('error'))
    <script>
      aalert('Error',"{!!Session::get('error')!!}");
    </script>
  @endif
@yield('js')
</body>
</html>
