   <h2 class="product-page-logo">
          @if(Request::segment('1')=='brand')
      <a href="{{ route('home') }}"><img src="{{asset('/admin-assets/uploads'.'/'.$brand->logo)}}" height="38px" width="100px"></img></a>
      @elseif($filtype=='solution')
      <a href="{{ route('home') }}"><img src="/frontend-assets/assets/images/sat.jpg"  height="38px" width="100px"></img></a>
     @else
      <a  href="{{ route('home') }}"><img src="{{asset('/admin-assets/uploads'.'/'.$breadcrumbs[0]->logo)}}"  height="38px" width="100px"></img></a>
      
      @endif

  </h2>
  <header>
    <nav class="main-nav" role="navigation"> <!-- remove class fixed-top -->
      <div class="container">

        <!-- Mobile menu toggle button (hamburger/x icon) -->
        <input id="main-menu-state" type="checkbox" />
        <label class="main-menu-btn" for="main-menu-state">
          <span class="main-menu-btn-icon"></span> Toggle main menu visibility
        </label>

        <!-- <h2 class="nav-brand"><a href="index.html">STP</a></h2> -->

        <!-- Sample menu definition -->
        <ul id="main-menu" class="sm sm-clean float-left"> <!-- add class float-left -->
          <li><a href="/home">Home</a></li>
            @if($filtype=='brand')
          
          <li><a href="#">Products</a>
               @include('frontend.partials.prodnavitem',array('childs'=>$brands->where('parent_id',$bid),'brands'=>$brands,'filtype'=>'brand','bid'=>$bid))
          </li>
          @endif
          <li><a href="#">Brands</a>
            <ul>
               @foreach($brands->where('parent_id',0) as $b)
              <li> <a href="{{URL::to('/brand'.'/'.$b->name)}}">{{$b->name}}</a></li>
              @endforeach
            </ul>
          </li>
          <li>
                <a >Solutions </a>
                      @include('frontend.partials.navitem',array('childs'=>$solutions->where('parent_id',0),'brands'=>$solutions,'filtype'=>'solution'))
          
          </li>
          
          <li><a href="{{ route('services') }}">Services</a></li>
          <li><a href="{{ route('gallery.albums') }}">Gallery</a></li>
          <li><a href="{{ route('about') }}">About</a></li>
          <li><a href="{{ route('contact') }}">Contact</a></li>
          <li><a href="{{ route('getQuote') }}" class="menu-quote">Get a Quote</a></li>
       </ul>
      </div> <!-- ./container -->
    </nav>
  </header>
