
  <header>
     <nav class="main-nav fixed-top" role="navigation">
      <div class="container">

        <!-- Mobile menu toggle button (hamburger/x icon) -->
        <input id="main-menu-state" type="checkbox" />
        <label class="main-menu-btn" for="main-menu-state">
          <span class="main-menu-btn-icon"></span> Toggle main menu visibility
        </label>

        <h2 class="nav-brand"><a href="/home"><img src="/frontend-assets/assets/images/sat.jpg"  height="24px" width="66px"></img></a></h2>

        <!-- Sample menu definition -->
        <ul id="main-menu" class="sm sm-clean">
          <li><a href="#">Brands</a>
            <ul>
               @foreach($brands->where('parent_id',0) as $b)
              <li> <a href="{{URL::to('/brand'.'/'.$b->name)}}">{{$b->name}}</a></li>
              @endforeach
            </ul>
          </li>
          <li><a href="#">Solutions</a>
              @include('frontend.partials.navitem',array('childs'=>$solutions->where('parent_id',0),'brands'=>$solutions,'filtype'=>'solution'))
                
          </li>
         
          <li><a href="{{ route('services') }}">Services</a></li>
          <li><a href="{{ route('gallery.albums') }}">Gallery</a></li>
          <li><a href="{{ route('about') }}">About</a></li>
          <li><a href="{{ route('contact') }}">Contact</a></li>
          <li><a href="{{ route('getQuote') }}" class="menu-quote">Get a Quote</a></li>
        </ul>
      </div> <!-- ./container -->
    </nav>
  </header>
  <div class="header-margin"></div>
