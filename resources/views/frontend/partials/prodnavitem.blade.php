<ul>
	@foreach($childs as $c)
   <li class="nav-item">

        @if($brands->where('parent_id',$c->id)->count()>0)
        	<a class="nav-link" href="{{URL::to('/products/?cid='.$c->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$c->name}}</a>
			@include('frontend.partials.prodnavitem',array('childs' => $brands->where('parent_id',$c->id),'brands'=>$brands,'bid'=>$bid))
		@else
				<a class="nav-link" href="{{URL::to('/products/?cid='.$c->id.'&filtype='.$filtype.'&bid='.$bid)}}">{{$c->name}}</a>
		
		@endif   
   </li>
   @endforeach
    
</ul>