
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">Service Support Message</h2>
            </div>
            <br>
        </div>
        <div class="content-body">
            <section class="card">
                <div id="invoice-template" class="card-block">
                    <!-- Invoice Company Details -->
                    <div id="invoice-company-details" class="row">
                        <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                            <ul class="px-0 list-unstyled" style="list-style: none;">
                                <li><strong>Name          : </strong>{{ $service_message->name }}</li>
                                <li><strong>Email         : </strong>{{ $service_message->email }}</li>
                                <li><strong>Phone No.     : </strong>{{ $service_message->phone }}</li>
                                <li><strong>Received Date :</strong> {{ date('M j, Y', strtotime($service_message->created_at)) }}</li>
                            </ul>
                        </div>
                    </div>
                    <!--/ Invoice Company Details -->

                    <!-- Invoice Footer -->
                    <div id="invoice-items-details" class="pt-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="text-align:center">
                                    <strong>Subject : </strong>{{ $service_message->subject }}
                                </div><br>
                                <p><strong>Message:</strong></p>
                                <p>{{ $service_message->message }}</p>
                            </div>
                        </div>
                    </div>
                    <!--/ Invoice Footer -->

                </div>
            </section>
        </div>
    </div>
</div>
