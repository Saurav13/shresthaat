
<form class="form-horizontal" role="form" name="modalForm">
    <div class="form-group" ng-class="{ 'has-error': modalForm.title.$invalid }">
        <label for="title">Title</label>
        <input type="text" name="title" class="form-control" placeholder="Enter title" ng-model="_category.name" required>
        <span ng-show="modalForm.title.$error.required" class="help-block">Input is required.</span>
    
    </div>
    <div class="form-group">
            <label for="details">Details</label>
            <textarea placeholder="Enter Details" name="details" ng-model=" _category.description" class="form-control"></textarea>
        
    </div>
    
   
    <div class="form-group">
        <button type="button" class="btn btn-success" data-dismiss="modal" ng-disabled="modalForm.$invalid" ng-class="{ 'disabled': modalForm.$invalid }" ng-click="addCategory(_category)">Add Category</button>
    </div>
</form>

