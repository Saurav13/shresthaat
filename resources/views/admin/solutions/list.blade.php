@extends('layouts.admin')

@section('css')

@endsection

@section('body')



   
<style>

.angular-ui-tree-handle {
    background: #f8faff;
    border: 1px solid #dae2ea;
    color: #7c9eb2;
    padding: 10px 10px;
}

.angular-ui-tree-handle:hover {
    color: #438eb9;
    background: #f4f6f7;
    border-color: #dce2e8;
}

.angular-ui-tree-placeholder {
    background: #f0f9ff;
    border: 2px dashed #bed2db;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

tr.angular-ui-tree-empty {
    height:100px
}

.group-title {
    background-color: #687074 !important;
    color: #FFF !important;
}


/* --- Tree --- */
.tree-node {
    border: 1px solid #dae2ea;
    background: #f8faff;
    color: #7c9eb2;
}

.nodrop {
    background-color: #f2dede;
}

.tree-node-content {
    margin: 10px;
    padding:5px;
}
.tree-handle {
    padding: 10px;
    background: #428bca;
    color: #FFF;
    margin-right: 10px;
}

.angular-ui-tree-handle:hover {
}

.angular-ui-tree-placeholder {
    background: #f0f9ff;
    border: 2px dashed #bed2db;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

</style>


 <div class="app-content content container-fluid" ng-app="treeapp" ng-controller="TreeController" ng-init="getCategory('solutions')">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">solutions</h2>
          </div>
 		</div>
 		<div class="card" >
 			
    <script type="text/ng-template" id="treeMenu">
        <div class="tree-node tree-node-content card-header">
              <div class="row">
            <div class="col-md-8">
            <h4 class="card-title"> <a href="javascript:;" class=" btn btn-success btn-sm" data-toggle="collapse" data-target="#demo@{{menuitem.id}}">
            <i class="icon-chevron-down2"></i>
           </a>&nbsp;&nbsp; @{{menuitem.name}}</h4>
           </div>
           
           <div class="col-md-4">
            <a title="add child" class="btn btn-outline-success btn-sm" data-nodrag ng-click="select('Add',menuitem.id,menuitem.parent_id)" data-toggle="modal" data-backdrop="static" data-target="#category">
                <i  class="icon-plus-circle" style="font-size:18px;"></i>
            </a>
            <a title="remove" class="btn btn-outline-danger btn-sm" ng-click="deleteCategory(menuitem.id)">
                <i class="icon-remove" style="font-size:18px;"></i>
            </a>
            <a title="edit" class="btn btn-outline-warning btn-sm" data-nodrag ng-click="select('Edit',menuitem.id,menuitem.parent_id)">
                <i class="icon-edit2" style="font-size:18px;"></i>
            </a>
            <a title="details" class="btn btn-outline-info btn-sm" data-nodrag ng-click="select('Details',menuitem.id,menuitem.parent_id)" data-toggle="modal" data-backdrop="static" data-target="#category" >
               <i class="icon-eye" style="font-size:18px;"></i>
            </a>
            </div>
            </div>
        </div>

        <ul style="list-style-type: none;" ng-if="(categorys | filter:{parent_id : menuitem.id}).length > 0" ng-class="{hidden: collapsed}" id="demo@{{menuitem.id}}" class="collapse">
            <li ng-repeat="menuitem in categorys | filter:{parent_id : menuitem.id} : true" ng-include="'treeMenu'"></li>
        </ul>

    </script>


    <div>
        <button class="btn btn-info btn-md" style="margin-top:10px; margin-left:10px;" data-toggle="modal" data-target="#category" data-backdrop="static" ng-click="select('AddSolution',0,0)">Add Solution</button>
        <br />
        <div class="row">
            <div class="col-sm-9">
                <ul style="list-style-type: none;" ui-tree id="tree-root">
                    <li ng-repeat="menuitem in categorys | filter:{parent_id : 0} : true" ng-include="'treeMenu'" class="accordion"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="modal fade" id="category" role="dialog">
        <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@{{action}}</h4>
                </div>
                <div class="modal-body">
                    <div ng-switch="action">
                        <div ng-switch-when="Details">@include('admin.solutions.partials.detailsCategory')</div>

                        <div ng-switch-when="Edit">@include('admin.solutions.partials.editCategory')</div>

                        <div ng-switch-when="Add">@include('admin.solutions.partials.addCategory')</div>
                             <div ng-switch-when="AddSolution">
                            <form class="form-horizontal" role="form" name="modalForm" method="POST" action="{{URL::to('/admin/solutions/createSolution')}}">
                                <div class="form-group">
                                    {{ csrf_field() }}
                                    <label for="title">Title</label>
                                    <input type="text" required name="name" class="form-control" placeholder="Enter Title" ng-model="_category.name" required>
                                    <label for="description">Description</label>
                                    <textarea class="form-control" placeholder="enter description" name="description"></textarea>
                                    <input type="hidden" name="parent_id" value="0"/>
                                </div>
                             
                                

                                <div class="form-group">
                                     <button  type="submit" class="btn btn-success" >Add</button>
                                
                                </div>
                            </form>

                          </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="x.dummy=''">Close</button>
                </div>
            </div>
        </div>
    </div>



 			
 		</div>
</div>
</div>

@endsection


@section('js')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
    <script src="{{asset('admin-assets/tree/js/app.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/basic-example.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/cloning.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/connected-trees.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/drop-confirmation.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/expand-on-hover.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/filter-nodes.js')}}"></script>
    <script src="{{asset('admin-assets/tree/js/table-example.js')}}"></script>
    <script src="{{asset('admin-assets/tree/tree.js')}}"></script>
    
@endsection