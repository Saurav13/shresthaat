 @extends('layouts.admin')

 @section('body')

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="linked-media-list">
                    <div class="row">
                        <div class="col-xs-12 mt-1">
                            <h4>General Service</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row match-height">
                        <div class="col-xs-12">
                            <div class="media-list media-bordered">
                                <?php $general_service = $services->find(1); ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title"><a data-action="collapse">{{ $general_service->title }} - {{$general_service->name}}</a></h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a href="{{ route('services.edit',$general_service->id) }}"><i class="icon-edit"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body collapse out">
                                        <div class="card-block">
                                            <div class="media">								
                                                <span class="media-left">
                                                    @if($general_service->photo)
                                                        <img class="media-object rounded-circle" src="{{ route('asset', ['service_images',$general_service->photo,200,200]) }}" alt="{{ $general_service->title }}" style="width:200px;height: 200px;" />
                                                    @else
                                                        <img class="media-object rounded-circle" src="{{ route('asset2', ['images','avatar.png','png',200,200]) }}" alt="{{ $general_service->title }}" style="width:200px;height: 200px;" />
                                                    @endif
                                                </span>
                                                <div class="media-body">
                                                    <strong>Name: </strong> {{ $general_service->name }}<br>
                                                    <strong>Email: </strong> {{ $general_service->email }}<br>
                                                    <strong>Address: </strong> {{ $general_service->address }}<br>
                                                    <strong>Contact: </strong> {{ $general_service->phone }}<br><br>

                                                    <h5 class="media-heading"><strong>{{ $general_service->title }}</strong></h5>
                                                    {!! $general_service->description !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </section>
                <hr>
                <!-- Media List start -->
                <section id="linked-media-list">
                    <div class="row">
                        <div class="col-xs-12 mt-1">
                            <h4>Other Services</h4>
                            <div class="text-xs-right" style="margin-top:-35px"><a class="btn btn-primary" href="{{ route('services.create') }}" >Add Service</a></div>
                            <hr>
                        </div>
                    </div>
                    <div class="row match-height">
                        <div class="col-xs-12">
                            <div class="media-list media-bordered">
                                @foreach($services as $service)
                                    @if($service->id == 1) @continue @endif
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title"><a data-action="collapse">{{ $service->title }} - {{$service->name}}</a></h4>
                                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="icon-plus4"></i></a></li>
                                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                    <li><a href="{{ route('services.edit',$service->id) }}"><i class="icon-edit"></i></a></li>
                                                    <li>
                                                        <form action="{{ route('services.destroy',$service->id) }}" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="_method" value="DELETE" >
                                                            <button id='deleteService' type="button" style="background-color: Transparent;border: none;outline:none;"><a><i class="icon-trash-o" style="font-size:20px"></i></a></button>

                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body collapse out">
                                            <div class="card-block">
                                                <div class="media">								
                                                    <span class="media-left">
                                                        @if($service->photo)
                                                            <img class="media-object rounded-circle" src="{{ route('asset', ['service_images',$service->photo,200,200]) }}" alt="{{ $service->title }}" style="width:200px;height: 200px;" />
                                                        @else
                                                            <img class="media-object rounded-circle" src="{{ route('asset2', ['images','avatar.png','png',200,200]) }}" alt="{{ $service->title }}" style="width:200px;height: 200px;" />
                                                        @endif
                                                    </span>
                                                    <div class="media-body">
                                                        <strong>Name: </strong> {{ $service->name }}<br>
                                                        <strong>Email: </strong> {{ $service->email }}<br>
                                                        <strong>Address: </strong> {{ $service->address }}<br>
                                                        <strong>Contact: </strong> {{ $service->phone }}<br><br>

                                                        <h5 class="media-heading"><strong>{{ $service->title }}</strong></h5>
                                                        {!! $service->description !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>

                        </div>
                    </div>
                </section>
                <!-- Media List end -->
            </div>
        </div>
    </div>
    
@endsection
