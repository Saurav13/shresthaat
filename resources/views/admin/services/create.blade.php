 @extends('layouts.admin')

 @section('body')

    <div class="app-content content container-fluid">
    
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Create New Service</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <form class="form" method="POST" action="{{ route('services.store') }}"  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @if ($errors->has('description'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="title">Service Title</label>
                                    <input type="text" class="form-control{{ $errors->has('title') ? ' border-danger' : '' }}" id="title" name="title" required>
                                    @if ($errors->has('title'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="name">Contact Person</label>
                                    <input class="form-control " id="name" type="text" class="form-control{{ $errors->has('name') ? ' border-danger' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput5">Email</label>
                                    <input class="form-control{{ $errors->has('email') ? ' border-danger' : '' }}" type="email" placeholder="Email"  name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput5">Address</label>
                                    <input class="form-control{{ $errors->has('address') ? ' border-danger' : '' }}" type="address" placeholder="Address"  name="address" required>
                                    @if ($errors->has('address'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput5">Contact Number</label>
                                    <input class="form-control{{ $errors->has('phone') ? ' border-danger' : '' }}" type="phone" placeholder="Contact"  name="phone" required>
                                    @if ($errors->has('phone'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">  
                                    <label for="photo">Photo <span class="text-muted">(Optional)</span></label>
                                    <input type="file" class="form-control{{ $errors->has('photo') ? ' border-danger' : '' }}" id="photo" name="photo">
                                    @if ($errors->has('photo'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea cols="50" rows="20" class="form-control{{ $errors->has('description') ? ' border-danger' : '' }}" id="description" name="description"></textarea>
                                    
                                </div>

                                <div class="form-actions right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>
		tinymce.init({
            selector: "textarea#description",
            theme: "modern",
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            toolbar2: "print preview | forecolor backcolor emoticons |",
        });
    </script>
   
@endsection