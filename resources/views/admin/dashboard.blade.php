solutions
 @extends('layouts.admin')

 @section('body')
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="pink">{{$totalProducts}}</h3>
                                        <span>Total Products</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-bag2 pink font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="teal">{{$totalQuotations}}</h3>
                                        <span>Quotations</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="deep-orange">{{$totalSupports}}</h3>
                                        <span>Support Request</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="cyan">{{$totalMessages}}</h3>
                                        <span>Messages</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-ios-help-outline cyan font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ stats -->
           
            <!-- Recent invoice with Statistics -->
            <div class="row match-height">
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-deep-orange media-left media-middle">
                                    <i class="icon-user1 font-large-2 white"></i>
                                </div>
                                <div class="p-2 media-body">
                                    <h5 class="deep-orange">Total Subscribers</h5>
                                    <h5 class="text-bold-400">{{$totalSubscribers}}</h5>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-cyan media-left media-middle">
                                    <i class="icon-camera7 font-large-2 white"></i>
                                </div>
                                <div class="p-2 media-body">
                                    <h5>Admin Staffs</h5>
                                    <h5 class="text-bold-400">{{$totalStaffs}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-deep-orange media-left media-middle">
                                    <i class="icon-user1 font-large-2 white"></i>
                                </div>
                                <div class="p-2 media-body">
                                    <h5 class="deep-orange">Total Services</h5>
                                    <h5 class="text-bold-400">{{$totalServices}}</h5>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Quotations</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <p>showing recent 5 quotations<span class="float-xs-right"><a href="/admin/quotations">view all <i class="icon-arrow-right2"></i></a></span></p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            <th>Received Date</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quotations as $q)
                                        <tr>
                                            <td class="text-truncate"><a href="#">QUO_{{$q->id}}</a></td>
                                            <td class="text-truncate">{{$q->customer_name}}</td>
                                            <td class="text-truncate"><span class="tag tag-default tag-success">{{$q->status?'read':'unread'}}</span></td>
                                            <td class="text-truncate">{{$q->created_at}}</td>
                                        </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Recent invoice with Statistics -->
       
                     <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Support Request</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <p>showing recent 5 support requests <span class="float-xs-right"><a href="/admin/service-support-messages">view all <i class="icon-arrow-right2"></i></a></span></p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            <th>Received Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($supportmessages as $s)
                                        <tr>
                                            <td class="text-truncate"><a href="#">SUP_{{$s->id}}</a></td>
                                            <td class="text-truncate">{{$s->name}}</td>
                                             <td class="text-truncate"><span class="tag tag-default tag-success">{{$q->status?'read':'unread'}}</span></td>
                                            <td class="text-truncate">{{$s->created_at}}</td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                     <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Messages</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <p>showing recent 5 messages <span class="float-xs-right"><a href="/admin/contact-us-messages">view all <i class="icon-arrow-right2"></i></a></span></p>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th>Status</th>
                                            <th>Received Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($messages as $s)
                                        <tr>
                                            <td class="text-truncate"><a href="#">MSG_{{$s->id}}</a></td>
                                            <td class="text-truncate">{{$s->name}}</td>
                                             <td class="text-truncate"><span class="tag tag-default tag-success">{{$q->status?'read':'unread'}}</span></td>
                                            <td class="text-truncate">{{$s->created_at}}</td>
                                        </tr>
                                        @endforeach
 
                                    </tbody>
                                </table>
                            </div>
                       
                    </div>
                </div>
          

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    
    
@endsection