 @extends('layouts.admin')

@section('body')
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">Quotation</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('quotations.index') }}">See all quotes</a>
                        <a class="btn btn-success mr-1 mb-1 " href="{{ route('quotations.reply',$quotation->id) }}" title="Reply">Reply</a>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="card">
                <div id="invoice-template" class="card-block">
                    <!-- Invoice Company Details -->
                    <div id="invoice-company-details" class="row">
                        <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                            <ul class="px-0 list-unstyled">
                                <li class="text-bold-800">{{ $quotation->customer_name }}</li>
                                {{--  <li>{{ $quotation->address }}</li>  --}}
                                <li>{{ $quotation->email }}</li>                                
                                <li>+977-{{ $quotation->phone_number }}</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-12 text-xs-center text-md-right">
                            <h2>QUOTATION</h2>
                            <p class="pb-3"># QUO-{{ 100+$quotation->id }}</p>
                            <ul class="px-0 list-unstyled">
                                <li><span class="text-muted">Invoice Date :</span> {{ date('M j, Y', strtotime($quotation->created_at)) }}</li>
                                
                            </ul>
                        </div>
                    </div>
                    <!--/ Invoice Company Details -->

                    <!-- Invoice Items Details -->
                    <div id="invoice-items-details" class="pt-2">
                        <div class="row">
                            <div class="table-responsive col-sm-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product Id</th>
                                            <th>Product Name</th>
                                            <th class="text-xs-right">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $items = json_decode($quotation->quote_items); ?>
                                        @foreach($items as $item)
                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td class="text-xs-right">{{ $item->quantity }}</td>
                                            </tr>
                                        @endforeach
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Invoice Footer -->
                    <div id="invoice-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="lead">Message:</p>
                                <p>{{ $quotation->message }}</p>
                            </div>
                        </div>
                    </div>
                    <!--/ Invoice Footer -->

                </div>
            </section>
        </div>
    </div>
</div>
@endsection
