 @extends('layouts.admin')

 @section('body')

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="text-xs-right">
                    @if($state == 'all')
                        <a class="btn btn-primary btn-min-width mr-2 mb-1 " href="{{ route('quotations.unseen') }}">See all unseen quotes</a>
                    @else
                        <a class="btn btn-primary btn-min-width mr-2 mb-1 " href="{{ route('quotations.index') }}">See all quotes</a>
                    @endif
                </div>
            </div>
            <div class="content-body">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Quotations</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer Name</th>
                                            <th>Quotation Code</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quotations as $q)
                                            @if(!$q->seen && $state == 'all')
                                                <tr style="background:#d6d3d3">
                                            @else
                                                <tr>
                                            @endif
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $q->customer_name }}</td>
                                                <td>QUO-{{ 100 + $q->id }}</td>
                                                <td>
                                                    <a class="btn btn-outline-info" title="View" href="{{ route('quotations.show',$q->id) }}"><i class="icon-eye"></i></a>
                            
                                                    <form action="{{ route('quotations.destroy',$q->id) }}" method="POST" style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE" >
                                                        <button id='deleteQuote' type="button" title="Delete" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @if(count($quotations)==0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No {{ $state == 'all'?'':'New'}} Quotations</p>
                                @endif
                                <div class="text-xs-center mb-3">
									<nav aria-label="Page navigation">
										{{ $quotations->links() }}
									</nav>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
   
   
@endsection