@extends('layouts.admin')

@section('body')
<link href="/admin-assets/dist/styles.imageuploader.css" rel="stylesheet" type="text/css">

 <div class="app-content content container-fluid" ng-app="ProductApp" ng-controller="ProductController" ng-init="editinit()">
<div class="content-wrapper">
<div class="content-header row">
<div class="content-header-left col-md-6 col-xs-12 mb-1">
<h2 class="content-header-title">Edit Details</h2>
</div>

</div>
<div class="row match-height">
<div class="col-xs-12">
<div class="card">
<div class="card-header">
	<h4 class="card-title">Enter Details</h4>
</div>
<input type="hidden" value="{{$product}}" id="product"/>

<div class="card-body">
	<div class="card-block">
	<ul class="nav nav-tabs nav-justified">
		<li class="nav-item">
			<a class="nav-link" id="basic-tab" data-toggle="tab" href="#basic" aria-controls="basic" aria-expanded="true">Basics</a>
		</li>
		<li class="nav-item">
			<a class="nav-link active" id="feature-tab" data-toggle="tab" href="#feature" aria-controls="feature" aria-expanded="false">Features</a>
		</li>
	
		<li class="nav-item">
			<a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" aria-controls="specification">Specifications</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="purpose-tab" data-toggle="tab" href="#purpose" aria-controls="purpose">Purpose</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="faq-tab" data-toggle="tab" href="#faq" aria-controls="faq">FAQ</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="driver-tab" data-toggle="tab" href="#driver" aria-controls="driver">Driver</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="images-tab" data-toggle="tab" href="#images" aria-controls="images">Images</a>
		</li>
	</ul>
	<div class="tab-content px-1 pt-1">
		<div role="tabpanel" class="tab-pane fade" id="basic" aria-labelledby="basic-tab" aria-expanded="true">
							
		<form class="form" ng-submit="basicEditSubmit()">
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i>Product Information</h4>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="modelname">Model Name</label>
							<input required ng-model="product.name" type="text" id="modelname" class="form-control" placeholder="Model Name" name="fname" value="{{$product->name}}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="brochure">Brochure</label>
							<input type="file" id="brochure" fileread="brochure" class="form-control" value="{{$product->brochure}}">
						</div>
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="description">Description</label>
					<textarea required ng-model="product.description" id="description" rows="5" class="form-control" name="comment" placeholder="About Product" value="{{$product->description}}"></textarea>
				</div>
					<h4 class="form-section"><i class="icon-head"></i>Priority</h4>
				<div class="form-group">
					<label for="issueinput5">Select Priority:<span>(1 for highest)</span></label>
					<select id="priority" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5" selected>5</option>
					</select>
				</div>
		
				
				<h4 class="form-section"><i class="icon-head"></i>Brand Category</h4>
				<div class="form-group">
					<label for="issueinput5">Select Category:</label>
					<select  ng-repeat="lvl in brandlevels" ng-model="lvl['model']" id="brandselect@{{lvl['level']}}" ng-change="brandchange(lvl['level'])" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option ng-repeat="l in lvl['vals']" value="@{{l.id}}">@{{l.name}}</option>
					</select>
				</div>

				<h4 class="form-section"><i class="icon-head"></i>Solution Category</h4>
				<div class="form-group">
					<label for="issueinput5">Select Category:</label>
					<select ng-repeat="lvl in solutionlevels" ng-model="lvl['model']" id="solutionselect@{{lvl['level']}}" ng-change="solutionchange(lvl['level'])" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option ng-repeat="l in lvl['vals']" value="@{{l.id}}">@{{l.name}}</option>
					</select>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" ng-disabled="basicSubmitDisabled()" class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>
		</div>


		<div class="tab-pane fade active in" id="feature" role="tabpanel" aria-labelledby="feature-tab" aria-expanded="false">
			<form class="form" ng-submit="featureSubmit()">
			<div class="form-body">
				<div ng-repeat="f in features">
						<h4 class="form-section"><i class="icon-head"></i> Feautre</h4>
					
				
					<div class="form-group">
							<label for="featuretitle" >Title</label>
							<input required type="text" ng-model="f.title" id="featuretitle" class="form-control" placeholder="Title" name="fname">
					</div>
					
 				
				
				<div class="form-group">
					<label for="featuredescription">Content</label>
					<textarea required id="featuredescription" ng-model="f.description" rows="5" class="form-control" name="comment" placeholder="Content"></textarea>
				</div>
					<button type="button" ng-click="removeFeature(f)" class="btn btn-danger">
					<i class="icon-remove"></i> Remove</button>
				<br>
				<br>
				<br>
				</div>
				<br>
				<button type="button" ng-click="addFeature()" class="btn btn-warning">
					<i class="icon-plus2"></i> Add Feautures
				</button>

			</div>

			<div class="form-actions">
				
				<button type="submit" class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>



		</div>
		
		<div class="tab-pane fade" id="specification" role="tabpanel" aria-labelledby="specification-tab" aria-expanded="false">
			<form class="form" ng-submit="saveSpecification()">
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i> Specification</h4>
				<div class="form-group">
					<label for="specification">Specifications</label>
					<textarea ui-tinymce id="tinymce1" ng-model="specification" row="50" column="50" class="form-control" placeholder="Specifications"></textarea>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>


		</div>
		<div class="tab-pane fade" id="purpose" role="tabpanel" aria-labelledby="purpose-tab" aria-expanded="false">
			<form class="form" ng-submit="savePurpose()">
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i>Purpose</h4>
				<div class="form-group">
					<label for="purpose">Purpose</label>
					<input name="image" type="file" id="upload" class="hidden" onchange="">

					<textarea ui-tinymce id="tinymce2" ng-model="purpose" rows="5" class="form-control" name="comment" placeholder="Product purpose and applications"></textarea>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit"  class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>
		</div>
		<div class="tab-pane fade" id="faq" role="tabpanel" aria-labelledby="faq-tab" aria-expanded="false">
			<form class="form" ng-submit="faqSubmit()" >
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i>FAQ</h4>
					<div ng-repeat="f in faqs">
					<div class="form-group">
							<label for="question">Question</label>
							<textarea id="question" ng-model="f.question" rows="5" required class="form-control" name="comment" placeholder="Write an answer"></textarea>
					</div>
					
				
				
				<div class="form-group">
					<label for="answer">Answer</label>
					<textarea id="answer" rows="5" ng-model="f.answer"  required class="form-control" name="answer" placeholder="Write an answer"></textarea>
				</div>
					<button type="button" ng-click="removeFaq(f)" class="btn btn-danger">
					<i class="icon-remove"></i> Remove</button>
				<br>
				<br>
				<br>
				</div>
				<button type="button" ng-click="addFaq()" class="btn btn-warning">
					<i class="icon-plus2"></i> Add More
				</button>

			</div>

			<div class="form-actions">
				
				<button type="submit" class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>

		</div>
		<div class="tab-pane fade" id="driver" role="tabpanel" aria-labelledby="driver-tab" aria-expanded="false">
		<form class="form" ng-submit="saveDriver(d)">
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i>Drivers</h4>
				<div ng-repeat="d in drivers">
				<div class="row" >
					<div class="col-md-4">
						<div class="form-group">
							<label for="dname">Driver Name</label>
							<input required ng-model="d.title" type="text" id="dname" class="form-control" placeholder="Driver Name">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="dfile">Driver</label>
							<text id="dfile" class="form-control">@{{d.title}}</text>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="driverfile">Driver</label>
							<input required type="file" id="driverfile" placeholder="d.title" fileread="d.file" class="form-control" value="@{{d.title}}">
						</div>
					</div>
				</div>

					<button type="button" ng-click="removeDriver(d)" class="btn btn-danger">
					<i class="icon-remove"></i> Remove</button>
					<button type="submit" class="btn btn-primary">
					<i class="icon-check2"></i> Save</button>
				<br>
				<br>
				<br>
				</div>
				<h4 class="form-section"><i class="icon-head"></i>Upload More</h4>
			
				<div class="row" >
					<div class="col-md-6">
						<div class="form-group">
							<label for="modelname">Driver Name</label>
							<input required ng-model="newdriver.title" type="text" id="modelname" class="form-control" placeholder="Model Name" name="fname">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="driverfile">Driver</label>
							<input required type="file" id="driverfile" fileread="newdriver.file" class="form-control">
						</div>
					</div>
				</div>
				<button type="button" ng-click="addDriver(newdriver)" class="btn btn-primary">
					<i class="icon-check2"></i> Upload</button>


			</div>
		</form>
		</div>

			<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab" aria-expanded="false">
		<div style="text-align:right;margin-bottom:15px;">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#uploadImagesModal">
                Add Images

                </button>

            </div>

            <div class="row match-height" id="productimages">
            	    <div class="col-xl-3 col-md-6 col-sm-12" ng-repeat="i in images">
                        <div class="card">
                            <div class="card-body">

                                <img class="card-img-top img-fluid" src="/admin-assets/uploads/@{{i.image}}" alt="Card image cap" style="widht:319px;height:160px">
                                <form ng-submit="deleteImage(i.id)" method="POST" style="text-align:center; margin-top:12px">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button id='deleteImage' type="submit" class="btn btn-danger btn-min-width mr-1 mb-1">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                
            </div>
    
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>


	<div class="modal fade text-xs-left" id="uploadImagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" style="min-height:500px;min-width:500px;">
        <div class="modal-dialog" role="document">

            <div style="text-align:right">             
                <button id="cross"  type="button" class="btn btn-outline-secondary" data-dismiss="modal" style="background-color: #185284;color: #fff;">&times;</button>
            </div>            
            <div class=" uploader__box js-uploader__box l-center-box">
                <form class="form" enctype="multipart/form-data" method="POST" action="{{ route('admin.settings.addImage') }}">
                       
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="uploader__contents">        
                            <label class="button button--secondary" for="fileinput">Select Files</label>
                            <input id="fileinput" class="uploader__file-input" type="file" multiple value="Select Files">
                        	</div>
                        <button class="button button--big-bottom" type="submit">Upload Selected Files</button>
                    </div>

                </form> 
            </div>
        </div>
    </div>
	@endsection

	@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>

	<script src="{{asset('admin-assets/product.js')}}"></script>
	  <script src="/admin-assets/dist/jquery.imageuploader.js"></script>
    <script src="/admin-assets/bootstrap3-editable/js/bootstrap-editable.js"></script>
	<script>
		(function(){
            var options = {'ajaxUrl':"/admin/products/addimage/{{$product->id}}",'reload':false};
            $('.js-uploader__box').uploader(options);
        }());
  </script>
 <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<!-- // <script src="/admin-assets/tinymce.js"></script> -->
	<script>
		// tinymce.init({
		// 	width:"900",
		// 	height:"400",
  //           selector: "textarea#tinymce1",
  //           theme: "modern",
  //           plugins: [
  //               "advlist autolink lists link charmap print preview hr anchor pagebreak",
  //               "searchreplace wordcount visualblocks visualchars code",
  //               "insertdatetime media nonbreaking save table contextmenu directionality",
  //               "emoticons paste textcolor colorpicker textpattern"
  //           ],
  //           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
  //           toolbar2: "print preview | forecolor backcolor emoticons"
  //       });
        // tinymce.init({
   //      	width:"900",
			// height:"400",
   //          selector: "textarea#tinymce2",
            
   //          plugins: [
   //              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
   //              "searchreplace wordcount visualblocks visualchars code",
   //              "insertdatetime media nonbreaking save table contextmenu directionality",
   //              "emoticons template paste textcolor colorpicker textpattern"
   //          ],
   //          toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
   //          toolbar2: "print preview | forecolor backcolor emoticons | template",
   //          image_advtab: true,
   //          file_picker_callback: function(callback, value, meta) {
   //          if (meta.filetype == 'image') {
   //              $('#upload').trigger('click');
   //              $('#upload').on('change', function() {
   //              var file = this.files[0];
   //              var reader = new FileReader();
   //              reader.onload = function(e) {
   //                  callback(e.target.result, {
   //                  alt: ''
   //                  });
   //              };
   //              reader.readAsDataURL(file);
   //              });
   //          }
   //          },
   //          templates: [
			//     {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
			//   ]
        // });

    </script>
   
	@stop