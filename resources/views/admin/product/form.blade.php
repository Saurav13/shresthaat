@extends('layouts.admin')

@section('body')
 <div class="app-content content container-fluid" ng-app="ProductApp" ng-controller="ProductController" ng-init="getBrandsAndSolutions()">
<div class="content-wrapper">
<div class="content-header row">
<div class="content-header-left col-md-6 col-xs-12 mb-1">
<h2 class="content-header-title">Add Product</h2>
</div>

</div>
<div class="row match-height">
<div class="col-xs-12">
<div class="card">
<div class="card-header">
	<h4 class="card-title">Enter Details</h4>
</div>
<div class="card-body">
	<div class="card-block">
	<ul class="nav nav-tabs nav-justified">
		<li class="nav-item">
			<a class="nav-link active" id="basic-tab" data-toggle="tab" href="#basic" aria-controls="basic" aria-expanded="true">Basics</a>
		</li>
		
	</ul>
	<div class="tab-content px-1 pt-1">
		<div role="tabpanel" class="tab-pane fade active in" id="basic" aria-labelledby="basic-tab" aria-expanded="true">
							
		<form class="form" ng-submit="basicSubmit()">
			<div class="form-body">
				<h4 class="form-section"><i class="icon-head"></i>Product Information</h4>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="modelname">Model Name</label>
							<input required ng-model="product.name" type="text" id="modelname" class="form-control" placeholder="Model Name" name="fname">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="brochure">Brochure</label>
							<input type="file" id="brochure" fileread="brochure" class="form-control">
						</div>
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="description">Description</label>
					<textarea required ng-model="product.description" id="description" rows="5" class="form-control" name="comment" placeholder="About Product"></textarea>
				</div>

				<h4 class="form-section"><i class="icon-head"></i>Priority</h4>
				<div class="form-group">
					<label for="issueinput5">Select Priority:<span>1 for highest</span></label>
					<select required id="priority" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5" selected>5</option>
					</select>
				</div>


				<h4 class="form-section"><i class="icon-head"></i>Brand Category</h4>
				<div class="form-group">
					<label for="issueinput5">Select Category:</label>
					<select  ng-repeat="lvl in brandlevels" ng-model="lvl['model']" id="brandselect@{{lvl['level']}}" ng-change="brandchange(lvl['level'])" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option ng-repeat="l in lvl['vals']" value="@{{l.id}}">@{{l.name}}</option>
					</select>
				</div>

				<h4 class="form-section"><i class="icon-head"></i>Solution Category</h4>
				<div class="form-group">
					<label for="issueinput5">Select Category:</label>
					<select ng-repeat="lvl in solutionlevels" ng-model="lvl['model']" id="solutionselect@{{lvl['level']}}" ng-change="solutionchange(lvl['level'])" name="priority" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
						<option ng-repeat="l in lvl['vals']" value="@{{l.id}}">@{{l.name}}</option>
					</select>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" ng-disabled="basicSubmitDisabled()" class="btn btn-primary">
					<i class="icon-check2"></i> Save
				</button>
			</div>
		</form>
		</div>



	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
	<script src="{{asset('admin-assets/product.js')}}"></script>
	@endsection