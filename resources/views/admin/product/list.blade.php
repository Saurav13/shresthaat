@extends('layouts.admin')

@section('body')
 <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title"><a href="{{URL::to('admin/products')}}" class="btn btn-success">All Products</a></h2>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><form action="{{URL::to('admin/products/search')}}" method="GET"><input type="text" name="key" placeholder="Search for Product" class="form-control"/><input type="submit" style="display: none" /> </form>
                </li>
                <li class="breadcrumb-item"><a href="{{URL::to('admin/products/create')}}" id="addproduct"class="btn btn-success">Add Product</a>
                </li>
              
                
              </ol>
            </div>
          </div>
        </div>
      
<div class="row">
@foreach($products as $p)
	<div class="col-xl-3 col-md-6 col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="card-block">
            @if($p->productimages->count()!=0)
						<img class="card-img img-fluid mb-1" src="{{ route('asset1', ['admin-assets','uploads',$p->productimages->first()->image,800,600]) }}" alt="{{asset('admin-assets/app-assets/images/carousel/08.jpg')}}">
						@else
            <img class="card-img img-fluid mb-1" src="{{asset('admin-assets/app-assets/images/carousel/08.jpg')}}">
            @endif
            <h4 class="card-title">{{$p->name}}</h4>
            <h6><a href="{{URL::to('/admin/products/changefeatured'.'/'.$p->id)}}"><i>({{!$p->featured?"Not Featured": "Featured"}})</i></a>
                <a href="{{URL::to('/admin/products/changestatus'.'/'.$p->id)}}"><i>({{$p->status}})</i></a>

            </h6>
						<p class="card-text">{{substr($p->description,0,60)}}...</p>
						
           <form action="{{ route('products.destroy',$p->id) }}" method="POST">
               <a href="{{URL::to('admin/products'.'/'.$p->id.'/'.'edit')}}" class="btn btn-outline-teal" >Edit</a>

                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE" >
                <button id='deleteProduct{{$p->id}}' type="button" class="btn btn-outline-danger">Delete</button>
           
            </form>
          </div>
				</div>
			</div>
		</div>
@endforeach
</div>

</div>
</div>
</div>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  
<script src="{{asset('jqueryTree/jquery.mjs.nestedSortable.js')}}"></script>
@endsection