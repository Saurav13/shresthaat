 @extends('layouts.admin')

 @section('body')

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="text-xs-right">
                    @if($state == 'all')
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('service-support-messages.unseen') }}">See all unseen messages</a>
                    @else
                        <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('service-support-messages.index') }}">See all messages</a>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Service Support Messages</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Subject</th>
                                            <th>Sent To</th>    
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($service_messages as $message)
                                        @if(!$message->seen && $state == 'all')
                                            <tr style="background:#d6d3d3">
                                        @else
                                            <tr>
                                        @endif
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $message->name }}</td>
                                            <td>{{ $message->subject }}</td>
                                            <td>{{ $message->sentTo }}</td>
                                            <td>
                                                <a class="btn btn-outline-info" title="View" href="{{ route('service-support-messages.show',$message->id) }}"><i class="icon-eye"></i></a>
                        
                                                <form action="{{ route('service-support-messages.destroy',$message->id) }}" method="POST" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE" >
                                                    <button id='deletemessage' type="button" title="Delete" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if(count($service_messages)==0)
                                    <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No {{ $state == 'all'?'':'New'}} Messages</p>
                                @endif
                                <div class="text-xs-center mb-3">
									<nav aria-label="Page navigation">
										{{ $service_messages->links() }}
									</nav>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
