
<form class="form-horizontal">
    <div class="form-group">
        <label for="detailtitle">Title:</label>
        <text class="form-control" id="detailtitle"> 
            @{{category.name}}
        </text>
    </div>
    <div class="form-group">
        <label for="detaildetail">Details:</label>
        <text class="form-control" id="detailtitle" style="overflow:auto; height:200px;">
            @{{category.description}}
        </text>
    </div>
    <div class="form-group" ng-show="pid==0">
        <label for="detaillogo">Logo</label>
        <img height="40px" width="100px" src="{{asset('admin-assets/uploads')}}/@{{category.logo}}"/>
    </div>
</form>
