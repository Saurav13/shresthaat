
<form class="form-horizontal" role="form" name="modalForm" style="margin-left: 160px;">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" class="form-control" placeholder="Enter title" ng-model="_category.name" required>
    
    </div>
    <div class="form-group"  ng-hide="id==0" ng-class="{ 'has-error': modalForm.details.$invalid }">
            <label for="details">Details</label>
            <textarea placeholder="Enter Details" name="details" ng-model=" _category.description" class="form-control" required></textarea>
            <span ng-show="modalForm.details.$error.required" class="help-block">Input is required.</span>
        
    </div>
    
  

    <div class="form-group">
        <button type="button" class="btn btn-success" data-dismiss="modal" ng-disabled="modalForm.$invalid" ng-class="{ 'disabled': modalForm.$invalid }" ng-click="addCategory(_category)">Add Category</button>
       
    </div>
</form>

