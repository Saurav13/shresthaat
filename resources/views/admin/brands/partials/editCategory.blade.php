<form class="form-horizontal" role="form">
    <div class="form-group">
        <label for="edittitle">Title:</label>
         <input type="text" name="edittitle" class="form-control" ng-model="category.name" value="@{{category.name}}">
       
    </div>
    <div class="form-group">
        <label for="description">Details:</label>
        <textarea id="description" rows="5" ng-model="category.description" value="@{{category.description}}" class="form-control" name="description" placeholder="About Project"></textarea>
    </div>
    <div class="form-group" ng-show="pid==0">
        <label for="Logo">Logo</label>
        <input type="file" fileread="category.editlogo" id="editlogo" class="form-control" placeholder="Last Name" name="lname">
    </div>                   
    <div class="form-group">
            <button type="button" class="btn btn-info" data-dismiss="modal" ng-click="editCategory(category)">Edit</button>
       </div>
</form>


