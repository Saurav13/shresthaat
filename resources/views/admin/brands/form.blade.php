@extends('layouts.admin')

@section('body')
<link href="/admin-assets/dist/styles.imageuploader.css" rel="stylesheet" type="text/css">

 <div class="app-content content container-fluid" ng-app="treeapp" ng-controller="TreeController" ng-init="getCategory('brands')">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Brands</h2>
          </div>
 		</div>
 		@if(count($errors)>0)

		    <div class="alert alert-danger no-border mb-2">
		        <strong>Errors</strong>
		        <ul>
		            @foreach($errors->all() as $error)
		            <li>{{$error}}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

 		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Enter Details</h4>
			</div>
			<div class="card-body">
			<div class="card-block">
			<ul class="nav nav-tabs nav-justified">
				<li class="nav-item">
					<a class="nav-link" id="basic-tab" data-toggle="tab" href="#basic" aria-controls="basic" aria-expanded="true">Basics</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" id="images-tab" data-toggle="tab" href="#images" aria-controls="images" aria-expanded="true">Images</a>
				</li>
		
				</ul>
			<div class="tab-content px-1 pt-1">
 				<div role="tabpanel" class="tab-pane fade" id="basic" aria-labelledby="basic-tab" aria-expanded="true">
					<form class="form" action="{{URL::to('/admin/brands/savebrand')}}" method="POST" files="true" enctype="multipart/form-data">
						<div class="form-body">
							<h4 class="form-section"><i class="icon-head"></i>Brand Detail</h4>
							<input type="hidden" value="{{$brand->id}}" name="id"/>
							{{csrf_field()}}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="modelname">Brand Name</label>
										<input required value="{{$brand->name}}" type="text" id="modelname" class="form-control" placeholder="Brand Name" name="name">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="modellogo">Brand Logo</label>
										<input type="file" id="modellogo" class="form-control" placeholder="Brand Logo" name="logo">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="description">Description</label>
								<textarea required id="description" rows="5" class="form-control" name="description" placeholder="About Brand">{{$brand->description}}</textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">
								<i class="icon-check2"></i> Save
							</button>
						</div>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane fade active in" id="images" aria-labelledby="images-tab" aria-expanded="true">
					<h4>Upload Some Images:</h4>
					<div style="text-align:right;margin-bottom:15px;">
		                <button type="button" class="btn btn-primary" data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#uploadImagesModal">
		                Add Images
		                </button>
		            </div>

		            <div class="row match-height" id="productimages">
	            	      @foreach($images as $image)
                            <div class="col-xl-3 col-md-6 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <img class="card-img-top img-fluid" src="{{ route('asset1', ['admin-assets','uploads',$image->image,319,160]) }}" alt="Card image cap" style="widht:319px;height:160px">
                                        <form action="{{ route('admin.brands.removeImage',$image->id) }}" method="POST" style="text-align:center; margin-top:12px">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" value="{{$brand->id}}" name="brand_id"/>
                        
                                            <button id='deleteImage' type="button" class="btn btn-danger btn-min-width mr-1 mb-1">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
	                </div>
				</div>

 			</div>
 			</div>
 			</div>
 		</div>
 	</div>
 </div>
    <div class="modal fade text-xs-left" id="uploadImagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true" style="min-height:500px;min-width:500px;">
        <div class="modal-dialog" role="document">

            <div style="text-align:right">             
                <button id="cross"  type="button" class="btn btn-outline-secondary" data-dismiss="modal" style="background-color: #185284;color: #fff;">&times;</button>
            </div>            
            <div class=" uploader__box js-uploader__box l-center-box">
                <form class="form" enctype="multipart/form-data" method="POST" action="">
                       
                    {{ csrf_field() }}

                    <div class="form-body">
                        <div class="uploader__contents">        
                            <label class="button button--secondary" for="fileinput">Select Files</label>
                            <input id="fileinput" class="uploader__file-input" type="file" multiple value="Select Files">
                        </div>
                        <button class="button button--big-bottom" type="submit">Upload Selected Files</button>
                    </div>

                </form> 
            </div>
        </div>
    </div>
@endsection

@section('js')
 <script src="/admin-assets/dist/jquery.imageuploader.js"></script>
    <script src="/admin-assets/bootstrap3-editable/js/bootstrap-editable.js"></script>
    
	<script>
		(function(){
            var options = {'ajaxUrl':"{{ route('admin.brands.addImage',$brand->id) }}"};
            $('.js-uploader__box').uploader(options);
        }());
      </script>
@endsection