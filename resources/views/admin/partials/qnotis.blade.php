@if($qno == 0)
    <p class="notification-text font-small-3 text-muted text-xs-center" style="margin:20px">No new quotations</p>
@endif
@foreach($quotes as $q)
    <?php 
        $pro_count = count(json_decode($q->quote_items)); 
        $interval = \Carbon\Carbon::createFromTimeStamp(strtotime($q->created_at))->diffForHumans();
    ?>
    <a href="{{ route('quotations.show',$q->id) }}" class="list-group-item">
        <div class="media">
            <div class="media-left valign-middle"><i class="icon-cart3 icon-bg-circle bg-cyan"></i></div>
            <div class="media-body">
            <h6 class="media-heading">You have new quotation Q{{ 100+$q->id }}!</h6>
            <p class="notification-text font-small-3 text-muted" style="line-height:1.2em">{{ $q->customer_name }} has requested a quotation for {{ $pro_count }} different types of products.</p><small>
                <time class="media-meta text-muted">{{ $interval }}</time></small>
            </div>
        </div>
    </a>
@endforeach