 @extends('layouts.admin')

 @section('body')
 <div data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"><!-- stats -->
                <section class="flexbox-container">
                    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
                        <div class="card-header bg-transparent pb-0">
                            <h2 class="error-code text-xs-center mb-2" style="font-size:10rem">403</h2>
                            <h3 class="text-uppercase text-xs-center">Access Denied/Forbidden !</h3>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection